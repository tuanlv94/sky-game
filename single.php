<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sky Game
 */

get_header(); ?>

<div <?php post_class( 'sky-container' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php sky_get_layout( 'content-blog' ); ?>

	<?php endwhile; // End of the loop. ?>
	
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
