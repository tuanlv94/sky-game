<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sky Game
 */

if ( ! is_active_sidebar( 'sky-single-sidebar' ) ) {
	return;
}
?>

<div class="<?php sky_sidebar_class(); ?> hidden-print">
	<div class="sky-sidebar-wrap">
		<?php if( is_singular( ) ) : ?>

			<?php dynamic_sidebar( 'sky-single-sidebar' ); ?>

		<?php elseif ( is_archive() ) : ?>

			<?php dynamic_sidebar( 'sky-cat-sidebar' ); ?>

		<?php endif; ?>
	</div><!-- /.sky-sidebar-wrap -->
</div><!-- .sky_sidebar_class() -->
