<?php

class Sky_Category extends WP_Widget {
	public function __construct() {
		parent::__construct (
			'sky_category',
			esc_html__('Sky Category','sky-game'),
			array ('description' => __ ( 'Display list categorys game', 'sky-game' ) )
		);
	}
	public function widget($args, $instance) {
		$title = apply_filters( 
			'widget_title', 
			empty( $instance['title'] ) ? '' : $instance['title'], 
			$instance, $this->id_base 
		);

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		// === Add code
		$taxonomy     = sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' );
		$orderby      = 'name'; 
		if ($instance['show_count']){
			$show_count = 1;
			$pad_counts = 1; 
		}else{
			$show_count = 0; 
			$pad_counts = 0; 
		}

		if ( $instance['show_hierarchy'] ) $hierarchical = 1;
		else $hierarchical = 0;

		if ( $instance['hide_empty'] ) $hide_empty = 1;
		else $hide_empty = 0;
		$title        = '';

		$sky_args = array(
			'taxonomy'         => $taxonomy,
			'orderby'          => $orderby,
			'echo' 			   => 0,
			'show_count'       => $show_count,
			'pad_counts'       => $pad_counts,
			'hierarchical'     => $hierarchical,
			'hide_empty'       => $hide_empty,
			'title_li'         => $title
		);
		?>

		<ul>
			<?php echo str_replace( array( '(', ')' ), array( '<span class="sky_badge">', '</span>' ), wp_list_categories( $sky_args ) ); ?>
		</ul>

		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 
			'title'            => '',
			// 'display_dropdown' => '',
			'show_count'       => '',
			'show_hierarchy'   => '',
			'hide_empty'   	   => '',
		) );
		$title            = $instance['title'];
		// $display_dropdown = $instance['display_dropdown'];
		$show_count       = $instance['show_count'];
		$show_hierarchy   = $instance['show_hierarchy'];
		$hide_empty   = $instance['hide_empty'];
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">
					<?php esc_html_e('Title:','sky-game'); ?> 
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
				</label>
			</p>
			<!-- <p>
				<input id="<?php echo $this->get_field_id( 'display_dropdown' ); ?>" name="<?php echo $this->get_field_name( 'display_dropdown' ); ?>" type="checkbox" value="true" <?php checked( $display_dropdown, 'true' ); ?> />
				<label for="<?php echo $this->get_field_id( 'display_dropdown' ); ?>"><?php esc_html_e( 'Display as dropdown','sky-game' ); ?></label>
			</p> -->
			<p>
				<input id="<?php echo $this->get_field_id( 'show_count' ); ?>" name="<?php echo $this->get_field_name( 'show_count' ); ?>" type="checkbox" value="true" <?php checked( $show_count, 'true' ); ?> />
				<label for="<?php echo $this->get_field_id( 'show_count' ); ?>"><?php esc_html_e( 'Show post counts','sky-game' ); ?></label>
			</p>
			<p>
				<input id="<?php echo $this->get_field_id( 'show_hierarchy' ); ?>" name="<?php echo $this->get_field_name( 'show_hierarchy' ); ?>" type="checkbox" value="true" <?php checked( $show_hierarchy, 'true' ); ?> />
				<label for="<?php echo $this->get_field_id( 'show_hierarchy' ); ?>"><?php esc_html_e( 'Show hierarchy','sky-game' ); ?></label>
			</p>
			<p>
				<input id="<?php echo $this->get_field_id( 'hide_empty' ); ?>" name="<?php echo $this->get_field_name( 'hide_empty' ); ?>" type="checkbox" value="true" <?php checked( $hide_empty, 'true' ); ?> />
				<label for="<?php echo $this->get_field_id( 'hide_empty' ); ?>"><?php esc_html_e( 'Hide Empty','sky-game' ); ?></label>
			</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 
			'title'               => '',
			// 'display_dropdown' => '',
			'show_count'          => '',
			'show_hierarchy'      => '',
			'hide_empty'          => '',
		));
		$instance['title']            = strip_tags($new_instance['title']);
		// $instance['display_dropdown'] = strip_tags($new_instance['display_dropdown']);
		$instance['show_count']       = strip_tags($new_instance['show_count']);
		$instance['show_hierarchy']   = strip_tags($new_instance['show_hierarchy']);
		$instance['hide_empty']   	  = strip_tags($new_instance['hide_empty']);
		return $instance;
	}
}
register_widget( 'Sky_Category' );
