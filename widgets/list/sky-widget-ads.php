<?php
 
class Sky_ADS_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct (
			'sky_ads_widget',
			esc_html__('Sky ADS','sky-game'),
			array ('description' => __ ( 'Displays the advertising.', 'sky-game' ) )
		);
	}

	public function widget($args, $instance) {  
		extract($args);
		
		$image    = $instance['image'];
		$url      = $instance['url'];
		$alt      = $instance['alt'];
		$code     = $instance['code'];
		$target   = !empty($instance['target']) ? ' target="_blank"' : '';
		$nofollow = !empty($instance['nofollow']) ? ' rel="nofollow"' : '';
		
        echo $before_widget;

		if ( $instance['title'] )
			echo $before_title . apply_filters( 'widget_title',  $instance['title'], $instance, $this->id_base ) . $after_title;
			
		echo '<div class="main-sky-widget-ads">';
		if(!empty($code))
			echo wp_kses_stripslashes($code);
		else
			echo '<a'.$target.$nofollow.' href="'.$url.'"><img src="'.$image.'" alt="'.$alt.'" /></a>';
		echo '</div>';
		
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {      
		$instance             = $old_instance;
		
		$instance             = $new_instance;
		$instance['target']   = isset($new_instance['target']) ? 1 : 0;
		$instance['nofollow'] = isset($new_instance['nofollow']) ? 1 : 0;
		
		return $instance;
	}

	function form($instance) {  
		// Defaults
		$defaults = array(
			'title'    => '',
			'image'    => '',
			'url'      => '',
			'text'     => '',
			'alt'      => '',
			'target'   => true,
			'nofollow' => true,
			'code'     => ''
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults);
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e( 'Title:', 'sky-game' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
        </p>
		<h4><?php esc_html_e( 'Image Ads', 'sky-game' ); ?></h4>
		<p>
            <label for="<?php echo $this->get_field_id('url'); ?>"><?php esc_html_e( 'Link URL:', 'sky-game' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('url'); ?>" value="<?php echo $instance['url']; ?>" class="widefat" id="<?php echo $this->get_field_id('url'); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('image'); ?>"><?php esc_html_e( 'Image URL:', 'sky-game' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('image'); ?>" value="<?php echo $instance['image']; ?>" class="widefat" id="<?php echo $this->get_field_id('image'); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('alt'); ?>"><?php esc_html_e( 'Alternate Text:', 'sky-game' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('alt'); ?>" value="<?php echo $instance['alt']; ?>" class="widefat" id="<?php echo $this->get_field_id('alt'); ?>" />
        </p>
			<p>
			<input type="checkbox" value="1" name="<?php echo $this->get_field_name('target'); ?>" id="<?php echo $this->get_field_id('target'); ?>" <?php checked($instance['target'], true); ?> />
			<label for="<?php echo $this->get_field_id('target'); ?>"><?php esc_html_e( 'Open link in new window or tab?',  'sky-game' ); ?></label>
		</p>
		<p>
			<input type="checkbox" value="1" name="<?php echo $this->get_field_name('nofollow'); ?>" id="<?php echo $this->get_field_id('nofollow'); ?>" <?php checked($instance['nofollow'], true); ?> />
			<label for="<?php echo $this->get_field_id('nofollow'); ?>"><?php esc_html_e( 'Add nofollow attribute to the link?',  'sky-game' ); ?></label>
		</p>
		
		<h4><?php esc_html_e( 'or Ads Code', 'sky-game' ); ?></h4>
		 <p>
            <label for="<?php echo $this->get_field_id('code'); ?>"><?php esc_html_e( 'Ad code:', 'sky-game' ); ?></label>
            <textarea name="<?php echo $this->get_field_name('code'); ?>" rows="10" class="widefat" id="<?php echo $this->get_field_id('code'); ?>"><?php echo $instance['code']; ?></textarea>
        </p>
        <?php
	}
}

register_widget('Sky_ADS_Widget');