<?php 
class Sky_Testimonial_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct (
			'sky_testimonial_widget', 		// Base ID
			esc_html__('Sky Testimonial','sky-game'), 		// Name
			array ('description' => __ ( 'Show list testimonial', 'sky-game' ) )
		);

		// === <<< Enqueue
			// wp_enqueue_style( 'carousel' );
			// wp_enqueue_style( 'carousel-theme' );
			// wp_enqueue_script( 'sky-carousel' );

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}
	public function widget($args, $instance) {

		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'sky_testimonial_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = apply_filters( 
			'widget_title', 
			empty( $instance['title'] ) ? '' : $instance['title'], 
			$instance, $this->id_base 
		);

		echo $args['before_widget'];
		if ( $title ) :
			echo $args['before_title'] . $title . $args['after_title'];
		endif;

		if( !empty($instance['sub_title']) ) :
			echo "<div class='sub_title'>{$instance['sub_title']}</div>";
		endif;

		// === <<< query
				$testimonial_args = array(
					
					'post_type'      => 'testimonial',
					'post_status'    => 'publish',
					'orderby'        => 'rand',
					'posts_per_page' => 3,
					
				);
			
			$wp_query = new WP_Query( apply_filters( 'widget_sky_testimonial_args', $testimonial_args ) );

			if ( $wp_query->have_posts() ) :
				
				?>
					<div class="testimonial_nav">
						<a class="btn prev"><i class="fa fa-angle-left"></i></a>
						<a class="btn next"><i class="fa fa-angle-right"></i></a>
					</div>
					<div class="testimonial_widget"><?php

					while ($wp_query->have_posts()): $wp_query->the_post(); global $post;
							$name     = get_post_meta(get_the_ID(),'sky_name', true);
							$position = get_post_meta(get_the_ID(),'sky_position', true);
							$url 	  = get_post_meta(get_the_ID(),'sky_image', true);
						?><div class="item_testimonial">
							
							<div class="say_testimonial"><?php the_content(); ?></div>
							<div class="info_testimonial">
								<div class="thumb">
									<img src="<?php echo wp_get_attachment_url(esc_attr($url)); ?>" alt="<?php the_title(); ?>" />
								</div>
								<div class="profile">
									<div class="name">
										<?php echo $name; ?>
									</div>
									<div class="position">
										<?php echo $position; ?>
									</div>
								</div>

							</div><!-- /.info_testimonial -->

						</div><!-- /.item_testimonial --><?php

					endwhile;
				?>
				</div><!-- /.testimonial_widget -->
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						var testimonial = $(".testimonial_widget");
					  	testimonial.owlCarousel({
							navigation : false, // Show next and prev buttons
							slideSpeed : 300,
							paginationSpeed : 400,
							singleItem:true,
							pagination: false, 
					  	});
					  	$(".next").click(function(){
						    testimonial.trigger('owl.next');
					  	})
						$(".prev").click(function(){
						    testimonial.trigger('owl.prev');
						})
					});
				</script>
				<?php

			// Reset the global $the_post as this query will have stomped on it
			wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'sky_testimonial_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 
			'title'     => '',
			'sub_title' => '',
		) );
		$title = $instance['title'];
		$sub_title = $instance['sub_title'];
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">
					<?php esc_html_e('Title','sky-game'); ?> 
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
				</label>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('sub_title'); ?>">
					<?php esc_html_e('Sub Title','sky-game'); ?> 
					<input class="widefat" id="<?php echo $this->get_field_id('sub_title'); ?>" name="<?php echo $this->get_field_name('sub_title'); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
				</label>
			</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 
			'title'     => '',
			'sub_title' => '',
		));
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['sub_title'] = strip_tags($new_instance['sub_title']);

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');
		
		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('sky_testimonial_widget', 'widget');
	}
}
register_widget( 'Sky_Testimonial_Widget' );
