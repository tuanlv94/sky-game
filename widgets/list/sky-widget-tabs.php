<?php
/**
 * Tabs Widget
*/
class Sky_Widget_Tabs extends WP_Widget {

	public function __construct() {

        parent::__construct(
            'sky_tabs',
            $name = esc_html__( 'Sky Tabs', 'sky-game' ),
            array(
                'description'	=> esc_html__( 'Displays Recent, Comments and Tags.', 'sky-game' )
            )
        );

    }


	public function sky_tabs_js() {
		wp_enqueue_script('sky-tabs-js', sky_plugin_url( 'assets/js/widgets/tabs.js' ), UW_VERSION );
	}

	public function widget( $args, $instance ) {
		extract( $args );

		$posts_number 	= $instance['posts_number']; ?>

		<div class="sky_tabs_widget">
			<div class="sky-top">
				<ul class="sky-posts-tabs sky-ul">
				<?php
					$tabs_order = 'r,c,t';
					if( !empty( $instance['tabs_order'] ) ){
						$tabs_order = $instance['tabs_order'];
					}
					$tabs_order_array = explode( ',' , $tabs_order );
					foreach ( $tabs_order_array as $tab ){
											
						if( $tab == 'r' ) {
							echo '<li class="sky-tabs"><a href="#sky-tab1">'. esc_html__( 'Game' ) .'</a></li>';	
						}
							
						if( $tab == 'c' ) {
							echo '<li class="sky-tabs"><a href="#sky-tab2">'. esc_html__( 'Blog' ) .'</a></li>';	
						}
							
						if( $tab == 't' ) {
							echo '<li class="sky-tabs"><a href="#sky-tab3">'. esc_html__( 'Tags' ) .'</a></li>';
						}
					}
				?>
				</ul>
			</div>
			
			<?php foreach ( $tabs_order_array as $tab ) {
			
			if( $tab == 'r' ) : ?>
				<div id="sky-tab1" class="sky-tabs-wrap">
					<ul>
						<?php sky_widget_show_post( $posts_number, 'game' ); ?>	
					</ul>
				</div>
			<?php endif; 
			
			if( $tab == 'c' ) : ?>
				<div id="sky-tab2" class="sky-tabs-wrap">
					<ul>
						<?php sky_widget_show_post( $posts_number );?>
					</ul>
				</div>
			<?php endif;
			
			if( $tab == 't' ) : ?>
				<div id="sky-tab3" class="sky-tabs-wrap sky-tagcloud">
					<?php wp_tag_cloud( $args = array( 'taxonomy' => array( 'post_tag', sky_get_option_setting( 'sky_general', 'tag_slug', 'sky-game-tags' ) ),'largest' => 8,'largest' => 12, 'number' => 25,'orderby'=> 'count', 'order' => 'DESC' )); ?>
				</div>
			<?php endif; } ?>

		</div>
	<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance 					= $old_instance;
		$instance['posts_number'] 	= strip_tags( $new_instance['posts_number'] );
		$instance['tabs_order'] 	= strip_tags( $new_instance['tabs_order'] );
		return $instance;
	}

	public function form( $instance ) {
		$sky_random_id 	= substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
		$id 			= explode("-", $this->get_field_id("widget_id"));
		$widget_id 		=  $id[1]. "-"  .$sky_random_id;
		$instance 		= wp_parse_args( (array) $instance, array( 'posts_number' => 5 )); ?>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$( "#<?php echo esc_attr($widget_id); ?>-order" ).sortable({
					placeholder: "placeholder",
					stop: function(event, ui) {
						var data = "";

						$( "#<?php echo esc_attr($widget_id) ?>-order li" ).each(function(i, el){
							var p = jQuery( this ).data( 'tab' );
							data += p+",";
						});

						$("#<?php echo esc_attr($widget_id) ?>-tabs-order").val(data.slice(0, -1));
					}
				});
			});
		</script>
		
		<div id="<?php echo esc_attr($widget_id) ?>-tabs">
			<p>
				<label for="<?php echo $this->get_field_id( 'tabs_order' ); ?>"><?php _e( 'Order Of Tabs:' , 'sky-game') ?></label>
				<?php if( $id[2] == '__i__' ) echo '<p style="background-color: #ffe9e9;padding: 5px;color: #D04544;border: 1px solid #E7A9A9;" class"sky_message_hint">'. esc_html__( "click Save button to be able to change the order of tabs ." , "kho").'</p>'?>
				
				<input id="<?php echo $widget_id ?>-tabs-order" name="<?php echo $this->get_field_name( 'tabs_order' ); ?>" value="<?php if( !empty($instance['tabs_order']) ) echo $instance['tabs_order']; ?>" type="hidden" />

				<ul id="<?php echo $widget_id ?>-order" class="tab_sortable" <?php if( $id[2] == '__i__' ) echo 'style="opacity:.5;"'?>>
				<?php
					$tabs_order = 'r,c,t';
					if( !empty( $instance['tabs_order'] ) ){
						$tabs_order = $instance['tabs_order'];
					}
					$tabs_order_array = explode( ',' , $tabs_order );
					foreach ( $tabs_order_array as $tab ){
			
						if( $tab == 'r' ) {
							echo '<li data-tab="r"> '. esc_html__( "Game" ) .' </li>';
						}
							
						if( $tab == 'c' ) {
							echo '<li data-tab="c"> '. esc_html__( "Blog" ) .' </li>';
						}
							
						if( $tab == 't' ) {
							echo '<li data-tab="t"> '. esc_html__( "Tags" ) .' </li>';
						}
					}
				?>
				</ul>
			</p>	
		</div>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'posts_number' ); ?>"><?php _e( 'Number Of Items To Show:' , 'sky-game') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'posts_number' ); ?>" name="<?php echo $this->get_field_name( 'posts_number' ); ?>" value="<?php if( !empty($instance['posts_number']) ) echo $instance['posts_number']; ?>" size="3" type="text" />
		</p>

	<?php
	}
}

register_widget('Sky_Widget_Tabs');