<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @package Sky Game
 */
function sky_game_widgets_init() {
	/**
	* Creates a sidebar
	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
	*/
	// -- [ Register home sidebar ] -- //
		register_sidebar( 
			array(
				'name'          => esc_html__( 'Sky Home Sidebar', 'sky-game' ),
				'id'            => 'sky-home-sidebar',
				'description'   => '',
				'before_widget' => '<aside id="%1$s" class="sky-widget widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="sky-widget-title widget-title">',
				'after_title'   => '</h3>',
			)
		);
	// -- [ Register single sidebar ] -- //
		register_sidebar( 
			array(
				'name'          => esc_html__( 'Sky Single Sidebar', 'sky-game' ),
				'id'            => 'sky-single-sidebar',
				'description'   => '',
				'before_widget' => '<aside id="%1$s" class="sky-widget widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="sky-widget-title widget-title">',
				'after_title'   => '</h3>',
			)
		);

	// -- [ Register category sidebar ] -- //
		register_sidebar( 
			array(
				'name'          => esc_html__( 'Sky Category Sidebar', 'sky-game' ),
				'id'            => 'sky-cat-sidebar',
				'description'   => '',
				'before_widget' => '<aside id="%1$s" class="sky-widget widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="sky-widget-title widget-title">',
				'after_title'   => '</h3>',
			)
		);

	// -- [ Register support sidebar ] -- //
		register_sidebar( 
			array(
				'name'          => esc_html__( 'Sky Other Sidebar', 'sky-game' ),
				'id'            => 'sky-other-sidebar',
				'description'   => '',
				'before_widget' => '<aside id="%1$s" class="sky-widget widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="sky-widget-title widget-title">',
				'after_title'   => '</h3>',
			)
		);

	// -- [ Register footer sidebar ] -- //
		$num = sky_get_customize_option('number_footer_column');
		if ( $num != 'none' ) :
			$i = 0; 
			while ( $i < $num ) : 
				$i++;
				register_sidebar( 
					array(
						'name'          => esc_html__( 'Sky Footer #' .$i, 'sky-game' ),
						'id'            => 'sky-footer-' .$i,
						'description'   => '',
						'before_widget' => '<aside id="%1$s" class="widget %2$s">',
						'after_widget'  => '</aside>',
						'before_title'  => '<h4 class="widget-title">',
						'after_title'   => '</h4><div class = "sky-small"></div>',
					)
				);
			endwhile;
		endif;

}
add_action( 'widgets_init', 'sky_game_widgets_init' );

sky_autoload( dirname(__FILE__) . '/list' );

// require (SKY_WIDGET .'/widgets.php');