<?php
/**
 * Template Name: Sky Demo 
 *
 *
 * @package Sky Game
 */
wp_head();
?>

<ul id="js-news" class="js-hidden"></ul>
<script type="text/javascript">
	
	jQuery(document).ready(function($) {
        $('#js-news').ticker({
            htmlFeed: false,
            ajaxFeed: true,
            feedUrl: 'http://money.vn/feed?post_type=sky-game',
            feedType: 'xml'
        });
	});

</script>
<?php
wp_footer();