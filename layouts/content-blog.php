<?php
/**
 * Template part for displaying game posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sky Game
 */

?>

<article id="post-<?php the_ID(); ?>" class="sky-xs-12 sky-md-8">
	<?php sky_breadcrumb(); ?>

	<?php if ( !sky_get_post_meta( get_the_ID(), 'sky_hide_title' ) ) : ?>
		
		<h1 id="entry-title">
			<?php the_title(); ?>
		</h1><!-- #entry-title -->

	<?php endif; ?>

	<div class="entry-content">

		<?php if ( !empty(sky_get_post_meta( get_the_ID(), 'sky_description' ) ) ) : ?>

			<?php echo "<h2 id=\"entry-description\">" . sky_get_post_meta( get_the_ID(), 'sky_description' ) . "</h2>"; ?>

		<?php endif; ?>

		<?php if ( !sky_get_post_meta( get_the_ID(), 'sky_hide_related' ) ) : ?>
			
			<div class="entry-related">
				<?php sky_get_related_posts(); ?>
			</div><!-- .entry-related -->

		<?php endif; ?>

		<div class="content-detail">
			<?php the_content(); ?>
		</div>

		<?php sky_add_social(); ?>

	</div><!-- .entry-content -->

	<?php if ( current_user_can( 'manage_options' ) ) : ?>
		<footer class="entry-footer">
			<?php sky_game_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>

	<div class="clearfix"></div>

	<?php if ( !sky_get_post_meta( get_the_ID(), 'sky_hide_comment_facebook' ) ) : ?>

		<div class="sky-comment"><?php esc_html_e( 'Bình luận', 'sky-game' ); ?></div>
		<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="5" data-width="100%"></div>

	<?php endif; ?>

</article><!-- #post-## -->
