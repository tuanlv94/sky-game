<?php
/**
 * Custom layouts.
 *
 * Display game
 *
 * @package Sky Game
 */

// -- check display layouts type
	if ( $display_type == 'related' ) :

		echo '<li class="sky-related-item">';
		echo '<a target="_blank" href="' . get_permalink( ) . '" class="tooltip" title="' . get_the_title() . '">';
		echo get_the_title();
		echo '</a>';
		echo '</li>';
	elseif ( $display_type == 'list' ) :

		?>
			<div class="sky-item">
					
				<div class="sky-thumb">
					
					<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">

						<img src="<?php echo sky_get_thumb( ); ?>" alt="<?php the_title( ); ?>" />

					</a>

				</div>

				<div class="sky-entry">
					
					<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">
						<h4>
							<?php the_title( ); ?>
						</h4>
					</a>

					<p>
						<?php esc_html_e( 'Hỗ trợ', 'sky-game' ); ?>&nbsp;
						<?php sky_get_list_support_os(); ?>
					</p>

					<a target="_blank" href="<?php echo sky_get_url_download() ?>" title="<?php esc_html_e( 'Tải về', 'sky-game' ); ?>">
						<span class="sky-btn-download">
							<i class="fa fa-download"></i>&nbsp; <?php esc_html_e( 'Tải về', 'sky-game' ); ?>
						</span>
					</a>

				</div>

			</div>
		<?php

	endif;
