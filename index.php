<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sky Game
 */

get_header(); ?>

<div <?php post_class( 'sky-container' ); ?>>
	<article id="post-<?php the_ID(); ?>" class="sky-xs-12 sky-md-8">
		<?php
			$type = array( 'post', 'sky-game' );
			$sky_args = array(
				'post_type'           => $type,
				'post_status'         => 'publish',
				'posts_per_page'      => 3,
				'ignore_sticky_posts' => 1
			);
			$wp_query = null;
			$wp_query = new WP_Query($sky_args);
		?>
		<?php if ( $wp_query->have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();  $display_type = 'list'; ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					include(locate_template("layouts/sky-game-loop.php"));
				?>

			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

			<?php sky_pagination(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	</article>
	<div class="sky-sidebar sky-xs-12 sky-md-4">
		<?php dynamic_sidebar( 'sky-home-sidebar' ); ?>
	</div>
	
</div>

<?php get_footer(); ?>
