jQuery(document).ready(function($) {
	$('.sky-widget').on('hover', 'li.cat-item', function(event) {
		event.preventDefault();
		$(this).children('.children').toggle('slow');
	});
	//Go to top
		$(window).scroll(function () {
			if ($(this).scrollTop() > 500) {
				$('.go-to-top').addClass('on');
			}
			else {
				$('.go-to-top').removeClass('on');
			}
		});
		$('body').on( 'click', '.go-to-top', function () {
			$("html, body").animate({
				scrollTop: 0
			}, 800);
			return false;
		});

	// ===== <<<Tooltip
	$( function()
	{
	    var targets = $( '.tooltip' ),
	        target  = false,
	        tooltip = false,
	        title   = false;
	  
	    targets.bind( 'mouseenter', function()
	    {
	        target  = $( this );
	        tip     = target.attr( 'title' );
	        tooltip = $( '<div id="tooltip"></div>' );
	  
	        if( !tip || tip == '' )
	            return false;
	  
	        target.removeAttr( 'title' );
	        tooltip.css( 'opacity', 0 )
	               .html( tip )
	               .appendTo( 'body' );
	  
	        var init_tooltip = function()
	        {
	            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
	                tooltip.css( 'max-width', $( window ).width() / 2 );
	            else
	                tooltip.css( 'max-width', 340 );
	  
	            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
	                pos_top  = target.offset().top - tooltip.outerHeight() - 20;
	  
	            if( pos_left < 0 )
	            {
	                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
	                tooltip.addClass( 'left' );
	            }
	            else
	                tooltip.removeClass( 'left' );
	  
	            if( pos_left + tooltip.outerWidth() > $( window ).width() )
	            {
	                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
	                tooltip.addClass( 'right' );
	            }
	            else
	                tooltip.removeClass( 'right' );
	  
	            if( pos_top < 0 )
	            {
	                var pos_top  = target.offset().top + target.outerHeight();
	                tooltip.addClass( 'top' );
	            }
	            else
	                tooltip.removeClass( 'top' );
	  
	            tooltip.css( { left: pos_left, top: pos_top } )
	                   .animate( { top: '+=10', opacity: 1 }, 50 );
	        };
	  
	        init_tooltip();
	        $( window ).resize( init_tooltip );
	  
	        var remove_tooltip = function()
	        {
	            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
	            {
	                $( this ).remove();
	            });
	  
	            target.attr( 'title', tip );
	        };
	  
	        target.bind( 'mouseleave', remove_tooltip );
	        tooltip.bind( 'click', remove_tooltip );
	    });
	});

	// ===== Live search
	$('.search_ajax_value').keyup(function(){
        var $_value = $(this).val();
        if( $_value.length >= 3 ){
            $('.search-input').addClass('eff_search');
            $('.returs_ajax').html('').removeClass('returs_ajax_aff');
            $.ajax({
               	url: skyManager.ajax_url,
               	type: 'post',
               	data: ({
	                action: 'sky_search_ajax_game',
	                title: $_value,
	            }),
               	success: function(data){
               	
                if(data){
                    $('.returs_ajax').html(data).addClass('returs_ajax_aff');
                    $('.search-input').removeClass('eff_search');
                }else{
                    $('.search-input').removeClass('eff_search');
                }
            }
        });
        }else{
            $('.returs_ajax').html('').removeClass('returs_ajax_aff');
            $('.search-input').removeClass('eff_search');
        }
    });
	// ====== <<<< ajax paging
		$('.sky_recent_post .sky-pagenavi').on('click', 'a', function(event) {
			event.preventDefault();
			// ===== <<<< get var
				var id_result = $(this).data( 'result' );
				var page = $(this).data( 'page' );
				var show = $('#'+ id_result).data('show');
				var category_post = $('#'+ id_result).data('category_post');
				var category_game = $('#'+ id_result).data('category_game');
				var column = $('#'+ id_result).data('column');
				var style = $('#'+ id_result).data('style');
				var posts_per_page = $('#'+ id_result).data('posts_per_page');
				var orderby = $('#'+ id_result).data('orderby');
				var show_pagination = $('#'+ id_result).data('show_pagination');
				var order = $('#'+ id_result).data('order');
				var show_readmore = $('#'+ id_result).data('show_readmore');
				var show_link_download = $('#'+ id_result).data('show_link_download');
				var show_excerpt = $('#'+ id_result).data('show_excerpt');
				var show_thumbnail = $('#'+ id_result).data('show_thumbnail');
				var show_support = $('#'+ id_result).data('show_support');
				var atts = [{
					'show' : show,
					'category_post' : category_post,
					'category_game' : category_game,
					'column' : column,
					'style' : style,
					'posts_per_page' : posts_per_page,
					'orderby' : orderby,
					'show_pagination' : show_pagination,
					'order' : order,
					'show_readmore' : show_readmore,
					'show_link_download' : show_link_download,
					'show_excerpt' : show_excerpt,
					'show_thumbnail' : show_thumbnail,
					'show_support' : show_support
				}];

			// ===== <<<< set current
				$('.' + id_result +' a').removeClass('current');
				$(this).addClass('current');
				
			// ===== <<<< set data
				var data = {
					action : 'recent_post',
					load_paging_ajax : true,
					paging : page,
					atts : atts,
				}
				$.post(skyProcess.ajax_url, data, function( res ) {
					$('#' + id_result).html( res );
				});
		});

});

// ==== Tabs Widget
var $j = jQuery.noConflict();

$j(document).ready(function() {
	"use strict";
	var tabs = $j(".sky_tabs_widget .sky-tabs-wrap");
		tabs.hide();
	$j(".sky_tabs_widget ul.sky-posts-tabs > li:first").addClass("sky-active").show();
	$j(".sky_tabs_widget .sky-tabs-wrap:first").show(); 
	$j(".sky_tabs_widget ul > li.sky-tabs").click(function() {
		$j(".sky_tabs_widget ul.sky-posts-tabs > li").removeClass("sky-active");
		$j(this).addClass("sky-active");
		tabs.hide();
		var activeTab = $j(this).find("a").attr("href");
		$j(activeTab).slideDown();
		return false;
	});
});

// === Require facebook
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// === Require twitter
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");