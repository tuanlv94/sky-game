jQuery(document).ready(function($) {
	$('#sky-manager').on('click', '.sky-manager-item', function(event) {
		event.preventDefault();
		var id = $(this).data('id');
		var action = $(this).data('action');

		// --- event edit
			if ( action === 'edit' ) {
				window.location.href = '/wp-admin/post.php?post='+ id +'&action=edit';
			}

		// --- event deleta
			if ( action === 'delete' ) {
				var answer = confirm(skyManager.notice);
				if ( answer ) {
					var data = {
						action : 'sky_manager_delete',
						security : skyManager.ajax_nonce,
						id : id
					}
					$.post(skyManager.ajax_url, data, function(data) {
						if ( data === 'ok' ) {
							window.location.href = '/';
						} else {
							alert(skyManager.error);
						}
					});
				}
			}

		// --- event image
			if ( action === 'img' ) {

				$('.sky-container-file').hide();
				$('.sky-container-image').toggle('fast');
				// --- Remove all background
					// $('#sky-manager .sky-manager-item').css('background', 'transparent');

				// --- Add background current
					// $(this).css('background', '#ddd');
					
				// ----
					$('.sky-container-image').on('click', 'span', function(event) {
						event.preventDefault();
						$('.process_reload').html('<i class="fa fa-refresh fa-spin"></i>');
						var ids = $(this).data('id');
						var actions = $(this).data('action');

						var datas = {
							action : 'sky_manager_process_image',
							security : skyManager.ajax_nonce,
							id : ids,
							v : actions,
							watermark_image : skyManager.watermark_image
						}

						$.post(skyManager.ajax_url, datas, function(data) {
							console.log(data);
							if ( data === 'ok' ) {
								location.reload(true);
								$('.process_reload').html();
							} else {
								$('.process_reload').html(skyManager.error);
							}
						});

					});

			}

		// --- event file
			if ( action === 'file' ) {

				$('.sky-container-image').hide();

				$('.sky-container-file').toggle('fast');
				
				// ==== <<
					$('.sky-container-file').on('click', 'span', function(event) {
						event.preventDefault();
						$('.process_reload').html('<i class="fa fa-refresh fa-spin"></i>');
						var ids = $(this).data('id');
						var actions = $(this).data('action');
						var link = $('#link_mod').text();
						if ( actions == 'mod' ) {
							var data_request = {
								mod : 'autoclick',
								security : skyManager.security,
								link : link,
								copyright : skyManager.copyright,
								site_attach : skyManager.site_attach,
								delete_app : skyManager.delete_app
							}
							$.ajax({
								url: skyManager.ajax_request,
								data: data_request,
							})
							.done(function( result ) {
								console.log( result );
							})
							.fail(function( result ) {
								console.log("error");
							})							

						}

					});

			}

	});
});