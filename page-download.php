<?php
/**
 * Template Name: Sky Download 
 *
 *
 * @package Sky Game
 */
$id = get_query_var( 'id_game' );
?>
<!doctype html>
<html lang="<?php language_attributes(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="vi"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="robots" content="noindex, nofollow"/>
    <title><?php esc_html_e( 'Tải ' . get_the_title( $id ), 'sky-game' ); ?></title>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            border: 0;
        }

        body {
            font-family: "Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;
            line-height: 1.5;
            font-size: 13px;
            background: #fff;
            padding: 0;
            color: #333333;
            word-wrap: break-word;
        }

        a {
            color: #0072FF;
            text-decoration: underline;
        }

        p {
            padding: 0;
            margin: 0;
        }

        table.files {

        }

        table.files td {
            padding: 10px;
            border-bottom: 1px solid #ddd;
        }

        .file-name {
            display: block;
        }

        .file-name a {
            font-weight: bold;
            text-decoration: none;
            color: #333333;
        }

        .file-description {
            color: #7B7B7B;
        }
    </style>
</head>
<body>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
	    <tbody>
	    <tr>
	        <td style="padding: 10px; width: 50px; ">
	        	<a href="<?php echo get_permalink( $id ); ?>" title="<?php echo get_the_title( $id ); ?>">
		            <img style="height: 50px;" src="<?php echo sky_get_thumb( $id ); ?>" alt="<?php echo get_the_title( $id ); ?>" title="<?php echo get_the_title( $id ); ?>">
		        </a>
	        </td>
	        <td style="padding: 10px 0; ">
	        	<a href="<?php echo get_permalink( $id ); ?>" title="<?php echo get_the_title( $id ); ?>">
		            <h1 style="font-size: 14px; margin: 0;">
		                <?php echo get_the_title( $id ); ?>
		            </h1>
	            </a>

	            <p style="color: #7B7B7B;">
	            	<?php 
	            		if ( !empty(sky_get_post_meta( $id, 'sky_description' ) ) ) :
							echo sky_get_post_meta( $id, 'sky_description' );
						else :
							echo get_the_excerpt( );
						endif; 
					?>
	            </p>
	        </td>
	        <td style="padding: 10px; width: 50px; ">
	        	<img src="http://api.qrserver.com/v1/create-qr-code/?size=50x50&amp;bgcolor=ffff00&amp;data=<?php echo wp_get_shortlink( $id ); ?>" alt="qrcode">
	            <!-- <img src="<?php echo sky_get_thumb(); ?>" style="width: 50px;"> -->
	        </td>
	    </tr>
	    </tbody>
	</table>
	<div style="border-bottom: 1px solid #ddd; border-top:1px solid #ddd; padding: 10px; background: #eee;">
		Bạn đang sử dụng <?php echo sky_get_browser(); ?>.<br />
		Để tải lại vui lòng chọn phiên bản tương ứng cho thiết bị của bạn dưới đây:
	</div>
    <div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="files">
            <tbody>
            <?php sky_get_list_download( null, 'download', $id ); ?>
            </tbody>
        </table>
    </div>
</body>
</html>
