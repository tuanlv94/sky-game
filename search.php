<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Sky Game
 */

get_header(); ?>
<div <?php post_class( 'sky-container' ); ?>>
	<article id="post-<?php the_ID(); ?>" class="sky-xs-12 sky-md-8">
		<?php sky_breadcrumb(); ?>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'sky-game' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); $display_type = 'list'; ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				include(locate_template("layouts/sky-game-loop.php"));
				?>

			<?php endwhile; ?>

			<?php sky_pagination(); ?>

		<?php else : ?>

			<?php include(locate_template("layouts/content-game.php")); ?>

		<?php endif; ?>
	</article>

	<?php get_sidebar(); ?>
	
</div>
<?php get_footer(); ?>
