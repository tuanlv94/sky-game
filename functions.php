<?php
/**
 * Sky Game functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package Sky Game
 */

/*
 * Setup global themes
 */
	if ( !defined( 'SKY_VER' ) ) {
		define( 'SKY_VER', '1.0.0' );
	}
	
	if ( !defined( 'SKY_THEME' ) ) {
		define( 'SKY_THEME', get_template_directory() );
	}
	
	if ( !defined( 'SKY_THEME_URI' ) ) {
		define( 'SKY_THEME_URI', get_template_directory_uri() );
	}
	
	if ( !defined( 'SKY_THEME_ASSETS_URI' ) ) {
		define( 'SKY_THEME_ASSETS_URI', SKY_THEME_URI . '/assets' );
	}
	
	if ( !defined( 'SKY_THEME_VENDOR_URI' ) ) {
		define( 'SKY_THEME_VENDOR_URI', SKY_THEME_ASSETS_URI . '/vendor' );
	}
	
	if ( !defined( 'SKY_THEME_INCLUDES' ) ) {
		define( 'SKY_THEME_INCLUDES', SKY_THEME . '/inc' );
	}
	
	if ( !defined( 'SKY_THEME_INCLUDES_URI' ) ) {
		define( 'SKY_THEME_INCLUDES_URI', SKY_THEME_URI . '/inc' );
	}
	
	if ( !defined( 'SKY_THEME_INCLUDES_FRAMEWORK' ) ) {
		define( 'SKY_THEME_INCLUDES_FRAMEWORK', SKY_THEME_INCLUDES . '/framework' );
	}
	
	if ( !defined( 'SKY_THEME_INCLUDES_FRAMEWORK_URI' ) ) {
		define( 'SKY_THEME_INCLUDES_FRAMEWORK_URI', SKY_THEME_INCLUDES_URI . '/framework' );
	}
	
	if ( !defined( 'SKY_THEME_INCLUDES_FUNCTION' ) ) {
		define( 'SKY_THEME_INCLUDES_FUNCTION', SKY_THEME_INCLUDES . '/functions' );
	}
	
	if ( !defined( 'SKY_THEME_INCLUDES_FUNCTION_URI' ) ) {
		define( 'SKY_THEME_INCLUDES_FUNCTION_URI', SKY_THEME_INCLUDES_URI . '/functions' );
	}
	
	if ( !defined( 'SKY_WIDGET' ) ) {
		define( 'SKY_WIDGET', SKY_THEME . '/widgets' );
	}
	
	if ( !defined( 'SKY_SET_THUMBNAIL_DEFAULT' ) ) {
		define( 'SKY_SET_THUMBNAIL_DEFAULT', SKY_THEME_ASSETS_URI . '/images/no-image.png' );
	}

	if ( ! defined( 'SKY_URL' ) ) {
		define( 'SKY_URL', SKY_THEME_INCLUDES_FRAMEWORK_URI . '/admin/' );
	}

	if ( ! defined( 'SKY_JS_URL' ) ) {
		define( 'SKY_JS_URL', trailingslashit( SKY_URL . 'js' ) );
	}

	if ( ! defined( 'SKY_CSS_URL' ) ) {
		define( 'SKY_CSS_URL', trailingslashit( SKY_URL . 'css' ) );
	}

	if ( ! defined( 'SKY_DIR' ) ){
		define( 'SKY_DIR', SKY_THEME_INCLUDES_FRAMEWORK . '/admin/' );
	}

	if ( ! defined( 'SKY_INC_DIR' ) ){
		define( 'SKY_INC_DIR', trailingslashit( SKY_DIR . 'inc' ) );
	}

	if ( ! defined( 'SKY_FIELDS_DIR' ) ){
		define( 'SKY_FIELDS_DIR', trailingslashit( SKY_INC_DIR . 'fields' ) );
	}


if ( ! function_exists( 'sky_game_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sky_game_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Sky Game, use a find and replace
		 * to change 'sky-game' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sky-game', SKY_THEME . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'html5', array( 'search-form' ) );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'sky-nav-bar' => esc_html__( 'Sky Meny Navbar', 'sky-game' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

	}
endif; // sky_game_setup
add_action( 'after_setup_theme', 'sky_game_setup' );

/* -------------------------------------------------------
 * Create functions sky_game_remove_default_image_sizes
 * Remove default image
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_game_remove_default_image_sizes' ) ) :
	
	function sky_game_remove_default_image_sizes( $sizes ) {

		// unset( $sizes['thumbnail']);
	    unset( $sizes['medium']);
	    unset( $sizes['large']);
	     
	    return $sizes;

	}

	add_filter('intermediate_image_sizes_advanced', 'sky_game_remove_default_image_sizes');

endif;

/** ====== END sky_game_remove_default_image_sizes ====== **/

/*
 * Load Sky Framework
 */
require SKY_THEME . '/inc/sky-theme.php';

/*
 * Custom widgets for theme
 */
require SKY_THEME . '/widgets/widgets_init.php';

/**
 * Visual Composer
 */
require_once SKY_THEME_INCLUDES_FRAMEWORK . '/admin/inc/vc_extension/vc_init.php';
