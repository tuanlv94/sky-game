<?php
/**
 * Sky Manager
 */

class Sky_Manager {

	public function __construct() {

		if ( current_user_can( 'manage_options' ) ) :

			require SKY_THEME_INCLUDES_FUNCTION . '/sky-process-image.php';

			add_action( 'wp_ajax_sky_manager_delete', array( $this, 'sky_manager_delete' ) );
			add_action( 'wp_ajax_sky_manager_process_image', array( $this, 'sky_manager_process_image' ) );
			add_action( 'wp_ajax_sky_manager_process_file', array( $this, 'sky_manager_process_file' ) );
			add_action( 'admin_menu', array( &$this, 'admin_menu' ) );

			add_action('sky_manager_general', array(&$this,'setting_general'));

			add_action( 'admin_init', array(&$this,'register_setting') );

		endif;

	}

	public function register_setting() {

		register_setting( 'sky_general', 'sky_general' );

	}

	public function admin_menu() {
		$hook = add_submenu_page( 'edit.php?post_type=sky-game', esc_html__( 'Settings', 'sky-game' ), esc_html__( 'Settings', 'sky-game' ), 'administrator', 'manage-sky-game', array( &$this, 'setting_page' ) );
	}

	public function setting_page() {
		$current_tab     = empty( $_GET['tab'] ) ? 'general' : sanitize_title( $_GET['tab'] );
		$tabs = apply_filters( 'sky_manager_tabs_array', array(
			'general'=>esc_html__('General','sky-game')
		));
		?>
		<div class="wrap">
			<form action="options.php" method="post">
				<h2 class="nav-tab-wrapper">
					<?php
						foreach ( $tabs as $name => $label )
							echo '<a href="' . admin_url( 'edit.php?post_type=noo_job&page=manage-sky-game&tab=' . $name ) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . $label . '</a>';
					?>
				</h2>
				<?php 
				do_action( 'sky_manager_' . $current_tab );
				submit_button(esc_html__('Save Changes','sky-game'));
				?>
			</form>
		</div>
		<?php
	}

	public function setting_general(){
			if(isset($_GET['settings-updated']) && $_GET['settings-updated']) {
				// flush_rewrite_rules();
			}
			$game_slug = sky_get_option_setting( 'sky_general', 'game_slug', 'game' );
			$category_game_slug = sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' );
			$offered_game_slug = sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' );
			$support_os_slug = sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' );
			$support_screen_slug = sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-support-screen' );
			$tag_slug = sky_get_option_setting( 'sky_general', 'tag_slug', 'sky-game-tags' );
			$testimonial_slug = sky_get_option_setting( 'sky_general', 'testimonial_slug', 'testimonials' );
			?>
			<?php settings_fields('sky_general'); ?>
			<h3><?php echo esc_html__('Sky General','sky-game')?></h3>
			<table class="form-table" cellspacing="0">
				<tbody>
					<tr>
						<th>
							<?php esc_html_e('Game Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[game_slug]" value="<?php echo $game_slug; ?>" size="70" />
						</td>
					</tr>
					<tr>
						<th>
							<?php esc_html_e('Category Game Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[category_game_slug]" value="<?php echo $category_game_slug; ?>" size="70" />
						</td>
					</tr>
					<tr>
						<th>
							<?php esc_html_e('Offered Game Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[offered_game_slug]" value="<?php echo $offered_game_slug; ?>" size="70" />
						</td>
					</tr>
					<tr>
						<th>
							<?php esc_html_e('Support OS Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[support_os_slug]" value="<?php echo $support_os_slug; ?>" size="70" />
						</td>
					</tr>
					<tr>
						<th>
							<?php esc_html_e('Support Screen Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[support_screen_slug]" value="<?php echo $support_screen_slug; ?>" size="70" />
						</td>
					</tr>
					<tr>
						<th>
							<?php esc_html_e('Tags Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[tag_slug]" value="<?php echo $tag_slug; ?>" size="70" />
						</td>
					</tr>
					<tr>
						<th>
							<?php esc_html_e('Testimonial Base (slug)','sky-game')?>
						</th>
						<td>
							<input type="text" name="sky_general[testimonial_slug]" value="<?php echo $testimonial_slug; ?>" size="70" />
						</td>
					</tr>
					<?php do_action( 'sky_manager_general_display_fields' ); ?>
				</tbody>
			</table>
			
			<?php 
		}

	public function sky_manager_delete() {

		check_ajax_referer( 'site-manager-script', 'security' );

		$id = sanitize_text_field( $_POST['id'] );

		wp_delete_post($id);

		echo 'ok';

		wp_die();

	}

	public function sky_manager_process_image() {

		// ---- Check security
			check_ajax_referer( 'site-manager-script', 'security' );

		// --- Process
			$id = sanitize_text_field( $_POST['id'] );
			$v = $_POST['v'];
			if ( $_POST['watermark_image'] ) :
				$image = wp_get_attachment_url( $_POST['watermark_image'] );
			else :
				$image = SKY_SET_THUMBNAIL_DEFAULT;
			endif;

		// --- Get content
			$content = get_post_field('post_content', $id);

		// --- Process image
			if ( isset($v) ) :

	    		$content = $this->sky_load_import_images( $id, $content, $image, $v );
	    		$update_post = array(
			     	'ID'           => $id,
			     	'post_content' => $content,
				);

				// --- Update the post into the database
				wp_update_post( $update_post );

	    	endif;

	    echo 'ok';
	    wp_die();

	}

	public function fetch_image($url) {
		if ( function_exists("curl_init") ) {
			return $this->curl_fetch_image($url);
		} elseif ( ini_get("allow_url_fopen") ) {
			return $this->fopen_fetch_image($url);
		}
	}

	public function curl_fetch_image($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$image = curl_exec($ch);
		curl_close($ch);
		return $image;
	}

	public function fopen_fetch_image($url) {
		$image = file_get_contents($url, false, $context);
		return $image;
	}

	public function sky_load_image_thumb( $post_id, $url_thumb, $show = null ) {
		$post = get_post( $post_id );
		if( empty( $post ) )
			return false;
		if( empty( $url_thumb ) )
			return false;	

		$imageurl = $url_thumb;
		$imageurl = stripslashes($imageurl);
		$uploads = wp_upload_dir();
		// $post_id = isset($_GET['post_id'])? (int) $_GET['post_id'] : 0;
		$ext = pathinfo( basename($imageurl) , PATHINFO_EXTENSION);
		$newfilename = sky_conver_vi_to_en(get_the_title( $post_id )) ? sky_conver_vi_to_en(get_the_title( $post_id )) . "." . $ext : basename($imageurl);
		
		$filename = wp_unique_filename( $uploads['path'], $newfilename, $unique_filename_callback = null );
		$wp_filetype = wp_check_filetype($filename, null );
		$fullpathfilename = $uploads['path'] . "/" . $filename;
		try {
			if ( !substr_count($wp_filetype['type'], "image") ) {
				throw new Exception( basename($imageurl) . ' is not a valid image. ' . $wp_filetype['type']  . '' );
			}
		
			$image_string = $this->fetch_image($imageurl);
			$fileSaved = file_put_contents($uploads['path'] . "/" . $filename, $image_string);
			if ( !$fileSaved ) {
				throw new Exception("The file cannot be saved.");
			}
			
			$attachment = array(
				 'post_mime_type' => $wp_filetype['type'],
				 'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
				 'post_content' => '',
				 'post_status' => 'inherit',
				 'guid' => $uploads['url'] . "/" . $filename
			);
			$attach_id = wp_insert_attachment( $attachment, $fullpathfilename, $post_id );
			if ( !$attach_id ) {
				throw new Exception("Failed to save record into database.");
			}
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$attach_data = wp_generate_attachment_metadata( $attach_id, $fullpathfilename );
			wp_update_attachment_metadata( $attach_id,  $attach_data );
			
			if ( $show == 'url' ) :
				$parsed = parse_url( wp_get_attachment_url( $attach_id ) );
				$url    = dirname( $parsed [ 'path' ] ) . '/' . rawurlencode( basename( $parsed[ 'path' ] ) );
				return $url;
			endif;
			return $attach_id;
			} catch (Exception $e) {
			$error = '<div id="message" class="error"><p>' . $e->getMessage() . '</p></div>';
		}
	}

	public function sky_load_import_images( $postid, $content, $image, $v = null ) {
		$content = str_replace('\"', '"', $content);
		preg_match_all( '#src="(.*?)"#is' , $content, $list_images);

		foreach ( array_unique($list_images[1]) as $img ) :
			if ( preg_match( '/^.*\.(jpg|jpeg|png|gif)$/i', $img ) ) :
				
				if ( preg_match( '/http:\/\//i', $img ) ) :

					if ( $v == 'import_watermark' ) :
						$w_img = "http://beta.gamedienthoai.info/watermark.php?url=http://gamedienthoai.info/wp-content/uploads/2015/09/21/Game-Rival-Kingdoms-Age-of-Ruin-v1-24-0-892-Hack-Full-Cho-Android-3.jpg&logo=http://skygame.mobi/wp-content/uploads/2015/10/logo-pc.png";
						$img_import = $this->sky_load_image_thumb( $postid, $w_img, 'url' );
					else :
						$img_import = $this->sky_load_image_thumb( $postid, $img, 'url' );
					endif;
					$content = str_replace( $img, $img_import, $content);

				endif;

			endif;

		endforeach;
		return $content;

	}

	public function sky_import_file( $postid, $file, $v = null ) {
		// === <<< get folder upload
			$upload_dir = wp_upload_dir();
		
		// === <<< Create folder Upload file
			$file_dir = $upload_dir['path'].'/sky-file';
			$file_url = $upload_dir['url'].'/sky-file';
			if( ! file_exists( $file_dir ) )
    			wp_mkdir_p( $file_dir );

    	// === <<< 
    		if ( !$file ) : return false;
    		else :
    			// === <<< Get basename
	    			$namefile = basename($file);
    			
    			// === <<< 
	    			$ex = explode( '.', $namefile );
    			
    			if( (end($ex) !='zip ') && ( end($ex)!='jar') && ( end($ex) != 'apk') ) :

    				return esc_html__( 'Invalid format (.zip, .jar)', 'sky-game' );

    			else :

    				$dir = "{$file_dir}/{$namefile}";
    				$url = "{$file_url}/{$namefile}";
					copy( $file, $dir);
					return $url;

    			endif;
    		endif;
	}

	public function sky_manager_process_file() {

		// ---- Check security
			check_ajax_referer( 'site-manager-script', 'security' );

		// --- Process
			$id = sanitize_text_field( $_POST['id'] );
			$v = $_POST['v'];
			$list_link = sky_get_post_meta( $id, 'sky_link_download' );


		// ---
			if ( $list_link ) :
				$list = array();
				foreach ( $list_link as $link ) :
					
					$info = explode('|', $link);
					$url_import = $this->sky_import_file( $id, $info[0] );
					unset( $info[0] );
					array_unshift($info, $url_import);
					$list = array_merge($list, array(implode('|', $info)));
					
				endforeach;
				
			endif;
		
	    update_post_meta( $id, 'sky_link_download', $list );
	    echo 'ok';
	    wp_die();

	}

}

new Sky_Manager();