<?php
/**
 * Sky Framework Site Package.
 *
 * Register Script
 * This file register & enqueue script used in Sky Theme.
 *
 * @package    Sky Game
 */

/* -------------------------------------------------------
 * Create functions sky_enqueue_site_script
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_enqueue_site_script' ) ) :
	
	function sky_enqueue_site_script() {
		wp_register_script( 'boostrap-vender', SKY_THEME_ASSETS_URI . '/vendor/bootstrap.min.js',  array('jquery'), '3.1.1', true );
		wp_enqueue_script( 'boostrap-vender' );
		wp_register_script( 'sky-script', SKY_THEME_ASSETS_URI . '/js/min/sky-script.min.js',  array( ), '1.0.0', true );
		wp_enqueue_script( 'sky-script' );

		// wp_register_script( 'superfish-vender', SKY_THEME_ASSETS_URI . '/vendor/superfish-1.7.4.min.js',  array('jquery'), '3.1.1', true );
		// wp_enqueue_script( 'superfish-vender' );
		
		wp_register_script( 'sky-carousel', SKY_THEME_VENDOR_URI . '/owl.carousel.min.js', array( 'jquery'), null, true );
		wp_enqueue_script( 'sky-carousel' );

		wp_register_script( 'sky-ticker', SKY_THEME_VENDOR_URI . '/news-ticker/jquery.ticker.js', array( 'jquery'), null, true );
		
		wp_register_script( 'site-manager-script', SKY_THEME_ASSETS_URI . '/js/min/site-manager-script.min.js',  array( 'jquery', 'plupload-html5' ), null, true );
		wp_enqueue_script( 'site-manager-script' );

		$sky_copyright_settings = get_option('sky_copyright_settings');

		wp_localize_script( 'site-manager-script', 'skyManager', 
			array( 
				'ajax_url'        => admin_url( 'admin-ajax.php' ),
				'ajax_request'    => 'http://update.skygame.mobi/tools/mod-java/index.php',
				'notice'          => esc_html__( 'Do you want to continue this action?', 'sky-game' ),
				'error'           => esc_html__( 'Error!', 'sky-game' ),
				'ajax_nonce'      => wp_create_nonce( 'site-manager-script' ),
				'security'        => wp_create_nonce( 'sky-manager' ),
				'site_attach'     => $sky_copyright_settings['url'],
				'copyright'       => $sky_copyright_settings['copyright'],
				'delete_app'      => $sky_copyright_settings['delete'],
				'watermark_image' => $sky_copyright_settings['watermark_image'],
			) 
		);

		wp_localize_script( 'sky-script', 'skyProcess', 
			array( 
				'ajax_url'      => admin_url( 'admin-ajax.php' ),
				'security'      => wp_create_nonce( 'sky-script' ),
				'img_ajax_load' => SKY_THEME_ASSETS_URI . '/images/ajax-loader.gif'
			) 
		);

	}

	add_action( 'wp_enqueue_scripts', 'sky_enqueue_site_script' );

endif;

/** ====== END sky_enqueue_site_script ====== **/

/* -------------------------------------------------------
 * Create functions sky_enqueue_admin_script
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_enqueue_admin_script' ) ) :
	
	function sky_enqueue_admin_script() {

		$depend = array('jquery', 'jquery-ui-core', 'jquery-ui-sortable', 'jquery-ui-slider');
		wp_enqueue_script( 'admin-script', SKY_THEME_ASSETS_URI . '/js/min/admin.min.js', $depend, '1.0.0', true );

		wp_localize_script( 'admin-script', 'skyAdmin', 
			array( 
				'ajax_url'         => admin_url( 'admin-ajax.php' ),
				'notice'           => esc_html__( 'Do you want to continue this action?', 'sky-game' ),
				'warning'          => esc_html__( 'Please waiting, not exit page.', 'domain' ),
				'ajax_nonce'       => wp_create_nonce( 'install-demo' ),
				'security'         => wp_create_nonce( 'sky-script-admin' ),
				'img_ajax_load'    => SKY_THEME_INCLUDES_FRAMEWORK_URI . '/admin/data/ajax-loader.gif',
				'file_spinner'     => get_option('sky_spinner', array() ),
				'spinner_deleted'  => get_option('sky_spinner_deleted', array() ), 
				'spinner_modified' => get_option('sky_spinner_modified', array() ), 
			) 
		);

	}

	add_action( 'admin_enqueue_scripts', 'sky_enqueue_admin_script' );

endif;

/** ====== END sky_enqueue_admin_script ====== **/

/* -------------------------------------------------------
 * Create functions sky_customizer_live_preview
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_customizer_live_preview' ) ) :
	
	function sky_customizer_live_preview() {

		wp_enqueue_script(
		    'sky-customizer',
		    SKY_THEME_ASSETS_URI .'/js/min/sky-live-customizer.js',
		    array( 'jquery','customize-preview' ),
		    '',
		    true
		);

	}
	
	// add_action( 'customize_preview_init', 'sky_customizer_live_preview' );

endif;

/** ====== END sky_customizer_live_preview ====== **/

/* -------------------------------------------------------
 * Create functions sky_load_script_on_footer
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_load_script_on_footer' ) ) :
	
	function sky_load_script_on_footer() {
		
		?>
			<script src="https://apis.google.com/js/platform.js" async defer></script>
		<?php

	}

	add_action( 'wp_footer', 'sky_load_script_on_footer' );

endif;

/** ====== END sky_load_script_on_footer ====== **/