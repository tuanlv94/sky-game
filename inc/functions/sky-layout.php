<?php
/**
 * Utilities Functions for SKY Framework.
 * This file contains various functions for getting and preparing data.
 *
 * @package    SKY Framework
 * @version    1.0.0
 */

if (!function_exists('get_page_layout')):
	function get_page_layout() {
		$layout = 'fullwidth';

		// Normal Page or Static Front Page
		if (is_page() || (is_front_page() && get_option('show_on_front') == 'page')) {
			// WP page,
			// get the page template setting
			$page_id = get_the_ID();
			$page_template = sky_get_post_meta($page_id, '_wp_page_template', 'default');
			
			if (strpos($page_template, 'sidebar') !== false) {
				if (strpos($page_template, 'left') !== false) {
					$layout = 'left_sidebar';
				}
				
				$layout = 'sidebar';
			}
		}


		// Index or Home
		if (is_home() || is_archive() || (is_front_page() && get_option('show_on_front') == 'posts')) {
			
			$layout = sky_get_option('sky_blog_layout', 'sidebar');
		}

		return apply_filters( 'sky_page_layout', $layout );
	}
endif;