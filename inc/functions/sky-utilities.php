<?php
/**
 * Utilities Functions for SKY Framework.
 * This file contains various functions for getting and preparing data.
 *
 * @package    SKY Framework
 * @version    1.0.0
 */

/* -------------------------------------------------------
 * Create functions sky_redirect_single_post
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_redirect_single_post' ) ) :
	
	function sky_redirect_single_post() {
		
		if (is_search()) {
	        global $wp_query;
	        if ($wp_query->post_count == 1) {
	            wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
	        }
	    }

	}

	add_action('template_redirect', 'sky_redirect_single_post');

endif;

/** ====== END sky_redirect_single_post ====== **/

/* -------------------------------------------------------
 * Create functions sky_set_query
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_set_query' ) ) :
	
	function sky_set_query() {
		
		$query  = esc_attr(get_search_query());
  
		if(strlen($query) > 0){
		    echo '
		      <script type="text/javascript">
		        var sky_query  = "'.$query.'";
		      </script>
		    ';
		}

	}

	add_action('wp_print_scripts', 'sky_set_query');

endif;

/** ====== END sky_set_query ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_all_post_type
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_all_post_type' ) ) :
	
	function sky_get_all_post_type() {

		$post_types = get_post_types( '', 'names' ); 
		$list = array();

		foreach ( $post_types as $post_type ) {

			if ( $post_type != 'attachment' && $post_type != 'revision' && $post_type != 'nav_menu_item' ) :
		    	
		    	$list[] = $post_type;

		    endif;
		
		}

		return $list;

	}

endif;

/** ====== END sky_get_all_post_type ====== **/


if (!function_exists('sky_get_page_id_by_template')):

	function sky_get_page_id_by_template( $page_template = '' ) {
		$pages = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => $page_template
		));

		if( $pages ){
			return $pages[0]->ID;
		}
		return false;
	}

endif;

if (!function_exists('sky_get_page_link_by_template')):

	function sky_get_page_link_by_template( $page_template ) {
		$pages = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => $page_template
		));

		if( $pages ){
			$link = get_permalink( $pages[0]->ID );
		}else{
			$link = home_url();
		}
		return $link;
	}

endif;

/* -------------------------------------------------------
 * Create functions sky_auto_add_nofollow
 * Auto add rel nofollow out link
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_auto_add_nofollow' ) ) :
	
	function sky_auto_add_nofollow( $content ) {
		
		$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>";
	
		if(preg_match_all("/$regexp/siU", $content, $matches, PREG_SET_ORDER)) :
			if( !empty($matches) ) :
				$srcUrl = get_option('siteurl');
				for ($i=0; $i < count($matches); $i++) :
					$tag = $matches[$i][0];
					$tag2 = $matches[$i][0];
					$url = $matches[$i][0];
					$noFollow = '';
					$pattern = '/rel\s*=\s*"\s*[n|d]ofollow\s*"/';
					preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
					if( count($match) < 1 )
						$noFollow .= ' rel="nofollow" ';
					$pos = strpos($url,$srcUrl);
					if ($pos === false) :
						$tag = rtrim ($tag,'>');
						$tag .= $noFollow.'>';
						$content = str_replace($tag2,$tag,$content);
					endif;
				endfor;
			endif;
		endif;
		
		$content = str_replace(']]>', ']]&gt;', $content);
		return $content;

	}

endif;

/** ====== END sky_auto_add_nofollow ====== **/

/* -------------------------------------------------------
 * Create functions sky_auto_add_tag_image
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_auto_add_tag_image' ) ) :
	
	function sky_auto_add_tag_image( $matches ) {
		
		global $post;
		$override= 'off';
		$override_title= 'off';
		$matches[0]=preg_replace('|([\'"])[/ ]*$|', '\1 /', $matches[0]);
		$matches[0] = preg_replace('/\s*=\s*/', '=', substr($matches[0],0,strlen($matches[0])-2));
		preg_match('/src\s*=\s*([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/', $matches[0], $source);
		$saved = $source[2];
		preg_match('%[^/]+(?=\.[a-z]{3}\z)%', $source[2], $source);
		$pieces = preg_split('/(\w+=)/', $matches[0], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		$image_name = (isset($source[0]) && !empty($source[0]) ) ? str_replace("-", " ", $source[0]) : '';
		if (!in_array('title=', $pieces) || $override_title=="on") {
			if (!in_array('title=', $pieces)) {
				array_push($pieces, ' title="' . sprintf( esc_html__( 'Image %s in %s.', 'sky-game' ), $image_name, $post->post_title ) . '"');
			} else {
				$key=array_search('title=',$pieces);
				$pieces[$key+1]= sprintf( esc_html__( 'Image %s in %s.', 'sky-game' ), $image_name, $post->post_title );
			}
		}
		
		if (!in_array('alt=', $pieces) || $override=="on" ) {
			
			if (!in_array('alt=', $pieces)) {
				array_push($pieces, ' alt="' . sprintf( esc_html__( 'Image %s in %s.', 'sky-game' ), $image_name, $post->post_title ) . '"');
			} else {
				$key=array_search('alt=',$pieces);
				$pieces[$key+1] = sprintf( esc_html__( 'Image %s in %s.', 'sky-game' ), $image_name, $post->post_title );
			}
		}
		return implode('', $pieces).' /';

	}

endif;

/** ====== END sky_auto_add_tag_image ====== **/

/* -------------------------------------------------------
 * Create functions seo_friendly_images
 * ------------------------------------------------------- */

if ( ! function_exists( 'seo_friendly_images' ) ) :
	
	function seo_friendly_images( $content ) {
		
		return preg_replace_callback('/<img[^>]+/', 'sky_auto_add_tag_image', $content);

	}

endif;

/** ====== END seo_friendly_images ====== **/


/* -------------------------------------------------------
 * Create functions sky_add_tag_image
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_add_tag_image' ) ) :
	
	function sky_add_tag_image( $content ) {

		$content = preg_replace( '/<img(.*?)>/is' , '<a href="#"><span class="roll"></span><img$1></a>', $content);
		return $content;

	}

endif;

/** ====== END sky_add_tag_image ====== **/


/* -------------------------------------------------------
 * Create functions sky_short_number
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_short_number' ) ) :
	
	function sky_short_number( $number, $decimals = 1, $suffix = '' ) {
		
		if( !$suffix ) $suffix = 'K,M,B';
		
		$suffix = explode(',', $suffix);
		
		if ($number < 1000) {
			$shorted = number_format($number);
		} elseif ($number < 1000000) {
			$shorted = number_format($number/1000, $decimals).$suffix[0];
		} elseif ($number < 1000000000) {
			$shorted = number_format($number/1000000, $decimals).$suffix[1];
		} else {
			$shorted = number_format($number/1000000000, $decimals).$suffix[2];
		}
		return $shorted;

	}

endif;

/** ====== END sky_short_number ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_thumb
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_thumb' ) ) :

	function sky_get_thumb( $post_ID = null , $meta_thumb = 'sky_thumbnail_url', $image_size = 'thumbnail' , $tag = false ) {
		
		if ( $post_ID ) :
			$post = get_post($post_ID);
			if ( get_query_var( 'id_game' ) ) : $post_ID = get_query_var( 'id_game' ); endif;
		else :
			global $post;
			$post_ID = $post->ID;
		endif;
		// === <<< Check value metabox
			if ( !empty(sky_get_post_meta( $post_ID, 'sky_thumbnail_url' )) ) :
				
				$post_thumbnail_src = sky_get_post_meta( $post_ID, 'sky_thumbnail_url' );

		// === <<< Check isset featured thumbnail
			elseif ( has_post_thumbnail( $post_ID ) ) :

				$thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($post_ID), $image_size);
				$post_thumbnail_src = $thumbnail_src[0];

		// === <<< get image in content
			else :

				$post_thumbnail_src = '';
				ob_start();
				ob_end_clean();
				$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/is', $post->post_content, $matches);
				if ( $output )
					$post_thumbnail_src = $matches[1][0];

			endif;

		// ==== <<< Checking isset image if not isset then set thumbnail default
			if( empty( $post_thumbnail_src ) ) :
				$post_thumbnail_src = SKY_SET_THUMBNAIL_DEFAULT;
			endif;

		return $post_thumbnail_src;

	}

endif;

/** ====== END sky_get_thumb ====== **/


/* -------------------------------------------------------
 * Create functions sky_set_post_views
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_set_post_views' ) ) :
	
	function sky_set_post_views( $id = null ) {
		// -- check single post or single page
			if ( !is_singular() ) return;

		// -- check id
			if ( empty( $id ) ) $id = get_the_ID();

		// -- create meta key
			$key_meta = '_sky_views_count';

		$count = sky_get_post_meta( $id, $key_meta );

		if ( $count == '' ) :
			$count = 1;
		else :
			$count ++;
		endif;
		update_post_meta( $id, $key_meta, $count );

	}
	add_action( 'wp_head', 'sky_set_post_views' );

endif;

/** ====== END sky_set_post_views ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_post_views
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_post_views' ) ) :
	
	function sky_get_post_views( $id = null ) {
		if ( empty( $id ) ) $id = get_the_ID();
		$key_meta = '_sky_views_count';
		$count = sky_get_post_meta( $id, $key_meta );
		if ( $count == '' ) :
			delete_post_meta( $id, $key_meta );
	        add_post_meta( $id, $key_meta, '0' );
	        return 0;
		endif;
		return sky_short_number($count);
	}

endif;

/** ====== END sky_get_post_views ====== **/

/* -------------------------------------------------------
 * Create functions sky_make_url_clickable
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_make_url_clickable' ) ) :
	
	function sky_make_url_clickable( $matches ) {

		$content = '';
	    $url = $matches[2];
	    if ( empty($url) )
	        return $matches[0];
	    // removed trailing [.,;:] from URL
	    if ( in_array(substr($url, -1), array('.', ',', ';', ':')) === true ) {
	        $content = substr($url, -1);
	        $url = substr($url, 0, strlen($url)-1);
	    }
	    return $matches[1] . "<a href=\"$url\" rel=\"nofollow\" title=\"$url\">$url</a>" . $content;

	}

endif;

/** ====== END sky_make_url_clickable ====== **/

/* -------------------------------------------------------
 * Create functions sky_make_ftp_clickable
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_make_ftp_clickable' ) ) :
	
	function sky_make_ftp_clickable( $matches ) {

		$content = '';
	    $dest = $matches[2];
	    $dest = 'http://' . $dest;
	    if ( empty($dest) )
	        return $matches[0];
	    // removed trailing [,;:] from URL
	    if ( in_array(substr($dest, -1), array('.', ',', ';', ':')) === true ) {
	        $content = substr($dest, -1);
	        $dest = substr($dest, 0, strlen($dest)-1);
	    }
	    return $matches[1] . "<a href=\"$dest\" rel=\"nofollow\" title=\"$dest\">$dest</a>" . $content;

	}

endif;

/** ====== END sky_make_ftp_clickable ====== **/

/* -------------------------------------------------------
 * Create functions sky_make_email_clickable
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_make_email_clickable' ) ) :
	
	function sky_make_email_clickable( $matches ) {

		$email = $matches[2] . '@' . $matches[3];
	    return $matches[1] . "<a href=\"mailto:$email\" title=\"mailto:$email\">$email</a>";

	}

endif;

/** ====== END sky_make_email_clickable ====== **/


/* -------------------------------------------------------
 * Create functions sky_make_clickable
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_make_clickable' ) ) :
  
  function sky_make_clickable( $content ) {

    $content = ' ' . $content;
    // in testing, using arrays here was found to be faster
    $content = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', 'sky_make_url_clickable', $content);
    $content = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', 'sky_make_ftp_clickable', $content);
    $content = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', 'sky_make_email_clickable', $content);
    // this one is not in an array because we need it to run last, for cleanup of accidental links within links
    $content = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $content);
    $content = trim($content);
    return $content;

  }

  add_filter( 'the_content', 'sky_make_clickable' );

endif;

/** ====== END sky_make_clickable ====== **/


/* -------------------------------------------------------
 * Create functions sky_substr
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_substr' ) ) :
	
	function sky_substr( $excerpt = null, $minword = 13 ) {

		if ( empty( $excerpt ) ) $excerpt = get_the_excerpt();
        $excerpt_ex = explode( ' ', $excerpt );
        $excerpt_slice = array_slice( $excerpt_ex, 0, $minword );
        $excerpt_content = implode( ' ', $excerpt_slice );
        return esc_html($excerpt_content) .'...';

	}

endif;

/** ====== END sky_substr ====== **/


/* -------------------------------------------------------
 * Create functions sky_conver_vi_to_en
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_conver_vi_to_en' ) ) :
	
	function sky_conver_vi_to_en( $str ) {

		$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
		$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
		$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
		$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
		$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
		$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
		$str = preg_replace("/(đ)/", 'd', $str);
		$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
		$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
		$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
		$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
		$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
		$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
		$str = preg_replace("/(Đ)/", 'D', $str);
		//$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
		return $str;

	}

endif;

/** ====== END sky_conver_vi_to_en ====== **/



/* -------------------------------------------------------
 * Create functions sky_get_url_download
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_url_download' ) ) :
	
	function sky_get_url_download( $post_ID = null ) {

		if ( empty($post_ID) ) $post_ID = get_the_ID();

		$post = get_post($post_ID);

		return home_url( ) . "/download/file-{$post_ID}";
	}

endif;

/** ====== END sky_get_url_download ====== **/



/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function sky_game_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'sky_game_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'sky_game_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so sky_game_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so sky_game_categorized_blog should return false.
		return false;
	}
}



/**
 * Flush out the transients used in sky_game_categorized_blog.
 */
function sky_game_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'sky_game_categories' );
}
add_action( 'edit_category', 'sky_game_category_transient_flusher' );
add_action( 'save_post',     'sky_game_category_transient_flusher' );



/* -------------------------------------------------------
 * Create functions sky_get_browser
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_browser' ) ) :

	function sky_get_browser( ) {
		require SKY_THEME_INCLUDES_FUNCTION . '/sky-mobile_detect.php';
	    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome;

		$md = new Mobile_Detect();
		$md->setDetectionType('extended');
		$browser = '';
		//Tablet, Mobile or Desktop
			if ( $md->isTablet() ) { $ss = '<b>Tablet</b> '; }
			else if ( $md->isMobile() ) { $ss = "<b>Mobile</b> "; }
			else { $ss = "<b>Desktop</b> "; }
			if ( isset($ss) ) $browser .= $ss;

		//Specific types of mobile devices
			if ( $md->isIphone() ) { $devices = '<b>Iphone</b> '; }
			if ( $md->isIpad() ) { $devices = '<b>Ipad</b> '; }
			if ( $md->isAndroidOS() ) { $devices = '<b>Android</b> '; }
			if ( $md->isBlackBerry() ) { $devices = '<b>Blackberry</b> '; }
			if ( $md->is('WindowsMobileOS') || $md->is('WindowsPhoneOS') ) { $devices = '<b>Windows Phone</b> '; }
			if ( $md->is('Samsung') ) { $devices = '<b>Samsung</b> '; }
			if ( $md->is('Kindle') ) { $devices = '<b>Kindle</b> '; }
			if ( $md->isiOS() ) { $devices = '<b>IOS</b> '; }
			if ( isset( $devices ) ) $browser = esc_html__( 'hệ điều hành ', 'sky-game' ) . $devices;
		
		//Browser specific
			if ($is_lynx) { $browser_specific = "<b>Lynx</b> "; }
			if ($is_gecko) { $browser_specific = "<b>Gecko</b> "; }
			if ($is_opera) { $browser_specific = "<b>Opera</b> "; }
			if ($is_NS4) { $browser_specific = "<b>Ns4</b> "; }
			if ($is_safari) { $browser_specific = "<b>Safari</b> "; }
			if ($is_chrome) { $browser_specific = "<b>Chrome</b> "; }
			if ($is_IE) { $browser_specific = "<b>IE</b> "; }
			if ( isset( $browser_specific ) ) $browser .= esc_html__( 'trên trình duyệt ', 'sky-game' ) . $browser_specific;
		return $browser;
	}

endif;

/** ====== END sky_get_browser ====== **/


/* -------------------------------------------------------
 * Create functions sky_excerpt_read_more
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_excerpt_read_more' ) ) :
	
	function sky_excerpt_read_more() {

		return '';

	}

	add_filter( 'excerpt_more', 'sky_excerpt_read_more' );

endif;

/** ====== END sky_excerpt_read_more ====== **/

/* -------------------------------------------------------
 * Create functions sky_content_read_more
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_content_read_more' ) ) :
	
	function sky_content_read_more() {

		return '';

	}

	add_filter( 'the_content_more_link', 'sky_content_read_more' );

endif;

/** ====== END sky_content_read_more ====== **/


/* -------------------------------------------------------
 * Create functions sky_autoload
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_autoload' ) ) :
	
	function sky_autoload( $dir = null ) {

		if ( empty( $dir ) ) $dir = dirname(__FILE__);
		foreach (scandir( $dir ) as $filename) {
            $path = $dir . '/' . $filename;
            if (is_file($path)) {
                require $path;
            }
        }

	}

endif;

/** ====== END sky_autoload ====== **/

/* -------------------------------------------------------
 * Create functions sky_add_lightbox_image
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_add_lightbox_image' ) ) :
	
	function sky_add_lightbox_image( $content ) {

		global $post;
        $pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
        $replacement = '<a$1href=$2$3.$4$5 rel="lightbox" title="'.$post->post_title.'"$6>';
        $content = preg_replace($pattern, $replacement, $content);
        return $content;

	}

	add_filter('the_content', 'sky_add_lightbox_image');

endif;

/** ====== END sky_add_lightbox_image ====== **/

/* -------------------------------------------------------
 * Create functions sky_auto_remove_width_height_image
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_auto_remove_width_height_image' ) ) :
	
	function sky_auto_remove_width_height_image( $html ) {

		$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
		return $html;

	}

	add_filter( 'post_thumbnail_html', 'sky_auto_remove_width_height_image', 10 );
	add_filter( 'image_send_to_editor', 'sky_auto_remove_width_height_image', 10 );

endif;

/** ====== END sky_auto_remove_width_height_image ====== **/

/* -------------------------------------------------------
 * Create functions sky_add_customizer_menu
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_add_customizer_menu' ) ) :
	
	function sky_add_customizer_menu() {

		add_submenu_page( 'sky-setup', esc_html__( 'Customizer', 'sky-game' ), esc_html__( 'Customizer', 'sky-game' ), 'manage_options', 'customize.php');

	}

endif;

/** ====== END sky_add_customizer_menu ====== **/

/* -------------------------------------------------------
 * Create functions sky_spiner_content
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_spiner_content' ) ) :
	
	function sky_spiner_content() {

		return preg_replace_callback(
	        '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
	        'sky_replace_spiner',
	        $text
	    );

	}

endif;

/** ====== END sky_spiner_content ====== **/

/* -------------------------------------------------------
 * Create functions sky_replace_spiner
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_replace_spiner' ) ) :
	
	function sky_replace_spiner() {

		$text = sky_spiner_content($text[1]);
	    $parts = explode('|', $text);
	    return $parts[array_rand($parts)];

	}

endif;

/** ====== END sky_replace_spiner ====== **/