<?php
/**
 * HTML Functions for SKY Framework.
 * This file contains various functions used for rendering site's small layouts.
 *
 * @package    SKY Framework
 * @version    1.0.0
 * @author     KEN
 * @copyright  Copyright (c) 2014, SkyTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 */

/* -------------------------------------------------------
 * Create functions sky_init_html
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_init_html' ) ) :
	
	function sky_init_html() {
		
		// ===== <<<< get option
		$option = get_option( 'sky_general_settings' );

		// =====
			if ( !isset( $option['disable_remove_version'] ) ) :
				add_filter( 'script_loader_src', 'sky_remove_version', 15, 1 );
				add_filter( 'style_loader_src', 'sky_remove_version', 15, 1 );
			endif;

		// =====
			if ( !isset( $option['disable_nofollow_out_link'] ) ) :
				add_filter( 'the_content', 'sky_auto_add_nofollow');
			endif;

		// =====
			if ( !isset( $option['disable_seo_friendly_images'] ) ) :
				add_filter('the_content', 'seo_friendly_images', 100);
			endif;

	}

	add_action( 'init', 'sky_init_html' );

endif;

/** ====== END sky_init_html ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_taxonomy_parents
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_taxonomy_parents' ) ) :
	
	function sky_get_taxonomy_parents( $id, $taxonomy = 'category', $link = false, $separator = ' &raquo; ', $nicename = false, $visited = array() ) {

		$chain = '';
        $parent = get_term( $id, $taxonomy );

        if ( is_wp_error( $parent ) )
            return $parent;

        if ( $nicename )
            $name = $parent->slug;
        else
            $name = $parent->name;

        if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
            $visited[] = $parent->parent;
            $chain .= sky_get_taxonomy_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
        }

        if ( $link )
            $chain .= '<a href="' . esc_url( get_term_link( $parent,$taxonomy ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
        else
            $chain .= $name.$separator;

        return $chain;

	}

endif;

/** ====== END sky_get_taxonomy_parents ====== **/


/* -------------------------------------------------------
 * Create functions sky_post_nav
 * ------------------------------------------------------- */

if ( ! function_exists('sky_post_nav') ) :
	function sky_post_nav() {
		global $post;

		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
		?>
		<?php $prev_link = get_previous_post_link( '%link', esc_html__( 'Previous post', 'sky-game' ) ); ?>
		<?php $next_link = get_next_post_link( '%link', esc_html__( 'Next post', 'sky-game' ) ); ?>
		<nav class="post-navigation<?php echo( (!empty($prev_link) || !empty($next_link) ) ? ' post-navigation-line':'' )?>" role="navigation">
			<div class="row">
				<div class="col-sm-6">			
				<?php if($prev_link):?>
					<div class="prev-post">
						<i class="fa fa-long-arrow-left">&nbsp;</i>
						<?php echo $prev_link?>
					</div>
				<?php endif;?>
				</div>
				<div class="col-sm-6">			
				<?php if(!empty($next_link)):?>
					<div class="next-post">
						<?php echo $next_link;?>
						<i class="fa fa-long-arrow-right">&nbsp;</i>
					</div>
				<?php endif;?>
				</div>
			</div>
		</nav>
		<?php
	}
endif;

/** ====== END sky_post_nav ====== **/

/* -------------------------------------------------------
 * Create functions sky_go_to_top
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_go_to_top' ) ) :
	
	function sky_go_to_top() {
		if( sky_get_customize_option( 'sky_back_to_top' ) ) {
			echo '<a href="#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>';
		}
		return ;

	}

	add_action( 'wp_footer', 'sky_go_to_top' );

endif;

/** ====== END sky_go_to_top ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_list_download
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_list_download' ) ) :
	
	function sky_get_list_download( $title = null, $display = null, $post_ID = null ) {

		if ( empty( $post_ID ) ) $post_ID = get_the_ID();

		$list_link = sky_get_post_meta( $post_ID, 'sky_link_download' );
		if ( $list_link ) :

			if ( $title ) echo '<div class="sky_download">' . $title . '</div><div class="sky_download_entry">';
			do_action( 'before_list_link_dowload' );
			
			if ( sky_get_post_meta( $post_ID, 'sky_before_link_download' ) ) :

				echo do_shortcode( sky_get_post_meta( $post_ID, 'sky_before_link_download_content' ) );

			endif;
			
			echo '<div class="sky_item_download">';
			echo '<img src="' . SKY_THEME_ASSETS_URI . '/images/icon_kaspersky.png" alt="' . esc_html__('Scan Kaspersky', 'sky-game') . '">';
			esc_html_e( 'Quét Virus: An toàn', 'sky-game' );
			echo ' <i class="fa fa-check" style="color: #00D714"></i></div>';
			$i = 1;
			foreach ( $list_link as $link ) :
				
				$info = array_map( 'trim', explode('|', $link) );
				if ( $i == 1 ) : echo '<div style="display: none;" id="link_mod">' . $info[0] . '</div>'; endif; $i++;
				$name_link = isset($info[1]) ? $info[1] : $info[0];
				if ( $display == 'download' ) :
					if ( isset($info[2]) ) $size = " <span style=\"color: red;font-size: 12px;\">{$info[2]}</span>";
					else $size = '';
				?>
					<tr>
	                    <td style="width: 30px;">
	                        <img src="<?php echo SKY_THEME_ASSETS_URI ;?>/images/icon_<?php echo $info[4]; ?>.png">
	                    </td>
	                    <td style="padding: 10px 0;">
	                        <div class="file-name">
	                            <a target="_blank" href="<?php echo $info[0] ?>" title="<?php echo $name_link; ?>">
	                                <?php echo $name_link; ?>
	                            </a>
	                            <?php echo $size; ?>
	                        </div>
	                        <p class="file-description">
	                        	<i>Dành cho các máy: </i>
	                         	Hệ điều hành: <b><?php echo $info[3]; ?></b>;
	                        </p>
	                    </td>
	                    <td style="width: 30px;">
	                        <a target="_blank" href="<?php echo $info[0] ?>" title="<?php echo $name_link; ?>">
	                            <img src="<?php echo SKY_THEME_ASSETS_URI ;?>/images/icon_download.png">
	                        </a>
	                    </td>
	                </tr>
				<?php
				else :

					if ( isset($info[2]) ) $size = " - <span class='sky_item_size'>{$info[2]}</span>";
					else $size = '';
					echo "<div class='sky_item_download'>";
					if ( $display == 'box' ) echo "<div class='sky_item_os'>{$info[3]}</div>";
					else echo "<i class='fa fa-download'></i>";
					echo "<a target='_blank' rel='nofollow' href='{$info[0]}' title='{$name_link}'>{$name_link}</a>{$size}";
					if ( $display == 'box' && isset($info[3])) :
						echo "<p>";
						esc_html_e( 'Dành cho các máy: Hệ điều hành: ' . "<strong>{$info[3]};</strong>", 'sky-game' );
						echo "</p>";
					endif;
					echo "</div>";

				endif;

			endforeach;
			do_action( 'after_list_link_dowload' );
			if ( $title ) echo "</div>";
		endif;

	}

endif;

/** ====== END sky_get_list_download ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_related_posts
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_related_posts' ) ) :
	
	function sky_get_related_posts( $game_ID = null ) {
		// -- get id
			if ( empty( $game_ID ) ) $game_ID = get_the_ID();

		// -- array query
			$args = array(
				'post_status'    => 'publish',
				'posts_per_page' => '5',
				'post__not_in'   => array( $game_ID )
			);
			$cat_ids = array();
			$display_type = 'related';
		
		// --- Checking post type
			if ( is_singular( 'post' ) ) :

				$args['post_type'] = 'post';

				// --- Tags
					$tags              = wp_get_post_terms( $game_ID, 'post_tag', ['fields' => 'ids'] );
					$args['tax_query'] = array( 'relation' => 'AND' );

					//  -- Tags Array
					
						$args['tax_query'][] = array(
							'taxonomy' => 'post_tag',
							'terms'    => $tags
						);

			elseif ( is_singular( 'sky-game' ) ) :

				$args['post_type'] = 'sky-game';

				//  -- tax_query
					$game_cat    = wp_get_post_terms( $game_ID, sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ) );
					$game_os     = wp_get_post_terms( $game_ID, sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ) );
					$game_screen = wp_get_post_terms( $game_ID, sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-support-screen' ) );
					$game_tags   = wp_get_post_terms( $game_ID, sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-tags' ) );

					$args['tax_query'] = array( 'relation' => 'AND' );

					//  -- Support Category
						if ( $game_cat ) {
							$term_game_cat = array(); 
							foreach ($game_cat as $cat) {
								$term_game_cat = array_merge( $term_game_cat, (array) $cat->slug );
							}
							$args['tax_query'][] = array(
								'taxonomy' => sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ),
								'field'    => 'slug',
								'terms'    => $term_game_cat
							);
						}
					//  -- End Support Category	

					//  -- Support OS
						if ( $game_os ) {
							$term_game_os = array(); 
							foreach ($game_os as $os) {
								$term_game_os = array_merge( $term_game_os, (array) $os->slug );
							}
							$args['tax_query'][] = array(
								'taxonomy' => sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ),
								'field'    => 'slug',
								'terms'    => $term_game_os
							);
						}
					//  -- End Support OS

					//  -- Support Screen
						if ( $game_screen ) {
							$term_game_screen = array(); 
							foreach ($game_screen as $screen) {
								$term_game_screen = array_merge( $term_game_screen, (array) $screen->slug );
							}
							$args['tax_query'][] = array(
								'taxonomy' => sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-support-screen' ),
								'field'    => 'slug',
								'terms'    => $term_game_screen
							);
						}
					//  -- End Support Screen

					//  -- Support Tags
						if ( $game_tags ) {
							$term_game_tags = array(); 
							foreach ($game_tags as $tags) {
								$term_game_tags = array_merge( $term_game_tags, (array) $tags->slug );
							}
							$args['tax_query'][] = array(
								'taxonomy' => sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-tags' ),
								'field'    => 'slug',
								'terms'    => $term_game_tags
							);
						}
					//  -- End Support Tags
					  
				//  -- end tax_query

			endif;	

		
		$wp_query = new WP_Query( $args );
 
		ob_start();
		if( $wp_query->have_posts() ) :
			echo '<ul class="entry-related">';
			while ( $wp_query->have_posts() ) : $wp_query->the_post(); global $post;
        		include(locate_template("layouts/sky-game-loop.php"));
        	endwhile;
        	echo '</ul><!-- .entry-related -->';
		endif;
        echo ob_get_clean();
		wp_reset_postdata();
		wp_reset_query();
 
	}

endif;

/** ====== END sky_get_related_posts ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_related_tag
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_related_tag' ) ) :
	
	function sky_get_related_tag() {
		
		global $post;
		if ( !empty(sky_get_post_meta( $post->ID, 'sky_key_related' )) ) :
			echo '<div id="entry-post-related">';
			$info = explode('|', sky_get_post_meta( $post->ID, 'sky_key_related' ));
			if ( !empty($info[0]) ) echo '<div id="entry-related-title">' . $info[0] . '</div>';
			if ( !empty($info[1]) ) $max = $info[1];
			else $max = 5;
			if ( !empty($info[2]) ) :
				$key = str_replace(' ', '+', $info[2]);
				$display_type = 'related';
				$wp_query = new WP_Query( array( 'posts_per_page'=> $max, 'tag' => $key ) );
				ob_start();
				echo '<ul class="item">';
				if( $wp_query->have_posts() ) :
					while ( $wp_query->have_posts() ) : $wp_query->the_post();
		        		include(locate_template("layouts/sky-game-loop.php"));
		        	endwhile;
				endif;
				echo '</ul>';
		        echo ob_get_clean();
				wp_reset_postdata();
				wp_reset_query();
			endif;
			echo '</div>';
		endif;

	}

endif;

/** ====== END sky_get_related_tag ====== **/

/**
 * Get a term's parents.
 *
 * @param    object $term Term to get the parents for.
 *
 * @return    array
 */
function get_term_parents( $term ) {
	$tax     = $term->taxonomy;
	$parents = array();
	while ( $term->parent != 0 ) {
		$term      = get_term( $term->parent, $tax );
		$parents[] = $term;
	}

	return array_reverse( $parents );
}

/**
 * Find the deepest term in an array of term objects
 *
 * @param  array $terms
 *
 * @return object
 */
	
function find_deepest_term( $terms ) {
	/*
	Let's find the deepest term in this array, by looping through and then
	   unsetting every term that is used as a parent by another one in the array.
	*/
	$terms_by_id = array();
	foreach ( $terms as $term ) {
		$terms_by_id[ $term->term_id ] = $term;
	}
	foreach ( $terms as $term ) {
		unset( $terms_by_id[ $term->parent ] );
	}
	unset( $term );

	/*
	As we could still have two subcategories, from different parent categories,
	   let's pick the one with the lowest ordered ancestor.
	*/
	$parents_count = 0;
	$term_order    = 9999; // Because ASC.
	reset( $terms_by_id );
	$deepest_term = current( $terms_by_id );
	foreach ( $terms_by_id as $term ) {
		$parents = get_term_parents( $term );

		if ( count( $parents ) >= $parents_count ) {
			$parents_count = count( $parents );

			// If higher count.
			if ( count( $parents ) > $parents_count ) {
				// Reset order.
				$term_order = 9999;
			}

			$parent_order = 9999; // Set default order.
			foreach ( $parents as $parent ) {
				if ( $parent->parent == 0 && isset( $parent->term_order ) ) {
					$parent_order = $parent->term_order;
				}
			}
			unset( $parent );

			// Check if parent has lowest order.
			if ( $parent_order < $term_order ) {
				$term_order   = $parent_order;
				$deepest_term = $term;
			}
		}
	}

	return $deepest_term;
}

/* -------------------------------------------------------
 * Create functions sky_show_video
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_show_video' ) ) :
	
	function sky_show_video( $post_ID = null ) {
		
		if ( empty( $post_ID ) ) :

			global $post;
			$post_ID = $post->ID;

		endif;

		?>
			<iframe width="100%" height="264" src="https://www.youtube.com/embed/<?php echo sky_get_post_meta( $post_ID, 'sky_video_url' ); ?>" frameborder="0" allowfullscreen></iframe>
		<?php

	}

endif;

/** ====== END sky_show_video ====== **/


/* -------------------------------------------------------
 * Create functions sky_breadcrumb
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_breadcrumb' ) ) :

	function sky_breadcrumb( ) {
		
		/* === OPTIONS === */
	    $text['home']     = esc_html__( 'Home', 'sky-game' );
	    $text['category'] = esc_html__( "%s", 'sky-game' );
	    $text['taxonomy'] = esc_html__( "%s", 'sky-game' );
	    $text['search']   = esc_html__( 'Search Results for "%s" Query', 'sky-game' );
	    $text['tag']      = esc_html__( 'Posts Tagged "%s"', 'sky-game' );
	    $text['author']   = esc_html__( 'Articles Posted by %s', 'sky-game' );
	    $text['404']      = esc_html__( 'Error 404', 'sky-game' );
	 
		$show_current        = 1; // 1 - show current page/category title in breadcrumbs, 0 - don't show
		$show_current_single = 0; // 1 - show current post title in breadcrumbs, 0 - don't show
		$show_on_home        = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
		$show_home_link      = 1; // 1 - show the 'Home' link, 0 - don't show
		$show_title          = 1; // 1 - show the title for the links, 0 - don't show
		$delimiter           = ' &raquo; '; // delimiter between crumbs
		$before              = '<span class="current">'; // tag before the current crumb
		$after               = '</span>'; // tag after the current crumb
	    /* === END OF OPTIONS === */
	 
	    global $post;
	    $home_link    = home_url('/');
	    $link_before  = '<span typeof="v:Breadcrumb">';
	    $link_after   = '</span>';
	    $link_attr    = ' rel="v:url" property="v:title"';
	    $link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
	    $parent_id    = $parent_id_2 = $post->post_parent;
	    $frontpage_id = get_option('page_on_front');
	 
	    if (is_home() || is_front_page()) {
	 
	        if ($show_on_home == 1) echo '<div id="sky-breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a></div>';
	 
	    } else {
	 
	        echo '<div id="sky-breadcrumbs">';
	        if ($show_home_link == 1) {
	            echo '<a href="' . $home_link . '" rel="v:url" property="v:title">' . $text['home'] . '</a>';
	            if ($frontpage_id == 0 || $parent_id != $frontpage_id) echo $delimiter;
	        }
	 
	        if ( is_category() ) {
	            $this_cat = get_category(get_query_var('cat'), false);
	            if ($this_cat->parent != 0) {
	            	
	                $cats = sky_get_taxonomy_parents($this_cat->term_id);
	                if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
	                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
	                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
	                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
	                echo $cats;
	            }
	            if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
	 
	        }elseif ( is_tax( sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ) ) ) {
	            
	            $this_tax = get_term_by( 'slug', get_query_var('term'), sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ));
	            if ($this_tax->parent != 0) {
	                $taxs = sky_get_taxonomy_parents( $this_tax->term_id, sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ), TRUE );
	                if ($show_current == 0) $taxs = preg_replace("#^(.+)$delimiter$#", "$1", $taxs);
	                $taxs = str_replace('<a', $link_before . '<a' . $link_attr, $taxs);
	                $taxs = str_replace('</a>', '</a>' . $link_after, $taxs);
	                if ($show_title == 0) $taxs = preg_replace('/ title="(.*?)"/', '', $taxs);
	                echo $taxs;
	            }
	            if ($show_current == 1) echo $before . sprintf($text['taxonomy'], single_cat_title('', false)) . $after;
	 
	        } elseif ( is_search() ) {
	            echo $before . sprintf($text['search'], get_search_query()) . $after;
	 
	        } elseif ( is_day() ) {
	            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
	            echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
	            echo $before . get_the_time('d') . $after;
	 
	        } elseif ( is_month() ) {
	            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
	            echo $before . get_the_time('F') . $after;
	 
	        } elseif ( is_year() ) {
	            echo $before . get_the_time('Y') . $after;
	 
	        } elseif ( is_single() && !is_attachment() ) {
	            if ( get_post_type() != 'post' ) {
	                $post_type = get_post_type_object(get_post_type());
	                $slug = $post_type->rewrite;
	                printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);

	                $term_list = wp_get_object_terms($post->ID, sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ));
	                if ( is_array( $term_list ) && $term_list !== array() ) :

						$deepest_term = find_deepest_term( $term_list );

						if ( is_taxonomy_hierarchical( sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ) ) && $deepest_term->parent != 0 ) {
							$parent_terms = get_term_parents( $deepest_term );
							foreach ( $parent_terms as $parent_term ) {
								echo $delimiter;
								printf($link, get_term_link( $parent_term ), $parent_term->name);
							}
						}
						echo $delimiter;
						printf($link, get_term_link( $deepest_term ), $deepest_term->name);

	                endif;

	                if ($show_current_single == 1) echo $delimiter . $before . get_the_title() . $after;
	            } else {
	                $cat = get_the_category(); $cat = $cat[0];
	                $cats = get_category_parents($cat, TRUE, $delimiter);
	                if ($show_current_single == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
	                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
	                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
	                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
	                echo $cats;
	                if ($show_current_single == 1) echo $before . get_the_title() . $after;
	            }
	 
	        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
	            $post_type = get_post_type_object(get_post_type());
	            echo $before . $post_type->labels->singular_name . $after;
	 
	        } elseif ( is_attachment() ) {
	            $parent = get_post($parent_id);
	            $cat = get_the_category($parent->ID); $cat = $cat[0];
	            $cats = get_category_parents($cat, TRUE, $delimiter);
	            $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
	            $cats = str_replace('</a>', '</a>' . $link_after, $cats);
	            if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
	            echo $cats;
	            printf($link, get_permalink($parent), $parent->post_title);
	            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
	 
	        } elseif ( is_page() && !$parent_id ) {
	            if ($show_current == 1) echo $before . get_the_title() . $after;
	 
	        } elseif ( is_page() && $parent_id ) {
	            if ($parent_id != $frontpage_id) {
	                $breadcrumbs = array();
	                while ($parent_id) {
	                    $page = get_page($parent_id);
	                    if ($parent_id != $frontpage_id) {
	                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
	                    }
	                    $parent_id = $page->post_parent;
	                }
	                $breadcrumbs = array_reverse($breadcrumbs);
	                for ($i = 0; $i < count($breadcrumbs); $i++) {
	                    echo $breadcrumbs[$i];
	                    if ($i != count($breadcrumbs)-1) echo $delimiter;
	                }
	            }
	            if ($show_current == 1) {
	                if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;
	                echo $before . get_the_title() . $after;
	            }
	 
	        } elseif ( is_tag() ) {
	            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
	 
	        } elseif ( is_author() ) {
	            global $author;
	            $userdata = get_userdata($author);
	            echo $before . sprintf($text['author'], $userdata->display_name) . $after;
	 
	        } elseif ( is_404() ) {
	            echo $before . $text['404'] . $after;
	        }
	 
	        if ( get_query_var('paged') ) {
	            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
	            echo __('Page') . ' ' . get_query_var('paged');
	            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
	        }
	 
	        echo '</div><!-- .breadcrumbs -->';
	 
	    }

	}

endif;

/** ====== END sky_breadcrumb ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_list_support_os
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_list_support_os' ) ) :

	function sky_get_list_support_os( $post_ID = null ) {
		
		// -- get id
			if ( empty( $post_ID ) ) $post_ID = get_the_ID();
		$args = array('orderby' => 'ID', 'order' => 'ASC', 'fields' => 'all');
		$list_os = wp_get_post_terms( $post_ID, sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ), $args );
		foreach ($list_os as $os) :
			
			$os_key = $os->taxonomy . '_' . $os->term_id;
			$os_meta = get_option( $os_key );
			echo '<a target="_blank" href="' . get_term_link( $os->slug, $os->taxonomy ) . '" title="'. $os->name . "\n" . $os->description .'">';
			echo '<i style="color: ' .$os_meta['color_icon'] . '" class="fa ' .$os_meta['os_icon'] . '"></i>&nbsp; ';
			echo '</a>';

		endforeach;

	}

endif;

/** ====== END sky_get_list_support_os ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_list_offered
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_list_offered' ) ) :

	function sky_get_list_offered( $post_ID = null ) {
		
		// -- get id
			if ( empty( $post_ID ) ) $post_ID = get_the_ID();

		$args = array('orderby' => 'ID', 'order' => 'ASC', 'fields' => 'all');
		$list_offered = wp_get_post_terms( $post_ID, sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' ), $args );
		$html = '';
		foreach ($list_offered as $offered) :
			
			$offered_key = $offered->taxonomy . '_' . $offered->term_id;
			$offered_meta = sky_get_option( $offered_key );
			$html .= '<a target="_blank" href="' . get_term_link( $offered->slug, $offered->taxonomy ) . '" title="'. $offered->name . "\n" . $offered->description .'">';
			$html .= $offered->name;
			$html .= '</a>, ';

		endforeach;

		return $html;
	}

endif;

/** ====== END sky_get_list_offered ====== **/


if ( ! function_exists( 'sky_game_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function sky_game_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'sky-game' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'sky-game' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;




if ( ! function_exists( 'sky_game_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function sky_game_entry_footer() {
	global $post;
	if ( is_singular( ) ) :

		?>

			<div id="sky-manager">
				
				<span data-id="<?php echo $post->ID; ?>" data-action="edit" class="sky-manager-item">
					<?php esc_html_e( 'Edit', 'sky-game' ); ?>
				</span>

				<span data-id="<?php echo $post->ID; ?>" data-action="delete" class="sky-manager-item delete">
					<?php esc_html_e( 'Delete', 'sky-game' ); ?>
				</span>

				<span data-id="<?php echo $post->ID; ?>" data-action="img" class="sky-manager-item">
					<?php esc_html_e( 'Images', 'sky-game' ); ?>
				</span>

				<span data-id="<?php echo $post->ID; ?>" data-action="file" class="sky-manager-item">
					<?php esc_html_e( 'File', 'sky-game' ); ?>
				</span>

				<span class="process_reload"></span>

				<div class="sky-container-image">
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="import" >
						<?php esc_html_e( 'Import', 'sky-game' ); ?>
					</span>
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="import_watermark" >
						<?php esc_html_e( 'Import &amp; Watermark', 'sky-game' ); ?>
					</span>
				</div>

				<div class="sky-container-file">
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="import" >
						<?php esc_html_e( 'Import', 'sky-game' ); ?>
					</span>
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="import_copyright" >
						<?php esc_html_e( 'Import &amp; Copyright (JAR)', 'sky-game' ); ?>
					</span>
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="mod" >
						<?php esc_html_e( 'Mod Game', 'sky-game' ); ?>
					</span>
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="jad" >
						<?php esc_html_e( 'Create JAD', 'sky-game' ); ?>
					</span>
					<span class="sky-triangle" data-id="<?php echo $post->ID; ?>" data-action="size" >
						<?php esc_html_e( 'Get Size', 'sky-game' ); ?>
					</span>
				</div>

			</div><!-- /#entry-manager -->

		<?php

	endif;

}
endif;




/* -------------------------------------------------------
 * Create functions sky_single_box_info
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_single_box_info' ) ) :

	function sky_single_box_info( ) {
		global $post;
		
		?>

			<div id="sky-box-info">
				
				<div id="sky-thumb">
					
					<img src="<?php echo sky_get_thumb( ); ?>" alt="<?php the_title( ); ?>" />

				</div><!-- /#sky-thumb -->

				<div id="sky-entry">
					
					<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">
						<h3>
							<?php the_title( ); ?>
						</h3>
					</a>
					<div id="sky-entry-data">
						<p>
							<?php if ( sky_get_post_meta( $post->ID, 'sky_size' ) ) : ?>
								<i class="fa fa-floppy-o"></i>&nbsp; <?php echo sky_get_post_meta( $post->ID, 'sky_size' ); ?>  | 
							<?php else : ?>
								<i class="fa fa-user"></i>&nbsp; 
								<?php echo sprintf( esc_html_x( '%s', 'post author', 'sky-game' ), '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>' ); ?> | 
							<?php endif; ?>
							<i class="fa fa-eye"></i>&nbsp; <?php echo sky_get_post_views(); ?>
						</p>
						<p>
							<?php esc_html_e( 'Hỗ trợ&nbsp;', 'sky-game' ); ?>
							<?php sky_get_list_support_os(); ?>
						</p>
						<a target="_blank" href="<?php echo sky_get_url_download() ?>" title="<?php esc_html_e( 'Tải về', 'sky-game' ); ?>">
							<span class="sky-btn-download">
								<i class="fa fa-download"></i>&nbsp; <?php esc_html_e( 'Tải về', 'sky-game' ); ?>
							</span>
						</a>
					</div>
					
				</div>
				
				<div id="sky-qr-code">	
					<img src="http://api.qrserver.com/v1/create-qr-code/?size=50x50&amp;bgcolor=ffff00&amp;data=<?php echo sky_get_url_download() ?>" alt="qrcode">
				</div>
				
			</div><!-- /#sky-box-info -->
		<?php

	}

endif;

/** ====== END sky_single_box_info ====== **/




/* -------------------------------------------------------
 * Create functions sky_pagination
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_pagination' ) ) :
	
	function sky_pagination( $pages = '', $range = 2, $id_result = null ) {
		$showitems = ($range * 1)+1;
		if( is_front_page() || is_home()) {
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
		} else {
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		}
		
		if($pages == ''){
			global $wp_query;
			$pages = $wp_query->max_num_pages;
			if(!$pages) $pages = 1;
		}
		if(1 != $pages){
			echo '<div class="sky-pagenavi ' . $id_result . '">';
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'" . ( !empty($id_result) ? ' data-result="' . $id_result . '"' : '' ) . " data-page='1' class='page larger'>«</a>";
			if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'" . ( !empty($id_result) ? ' data-result="' . $id_result . '"' : '' ) . " data-page='" . ($paged - 1) . "' class='page larger'>‹</a>";
			for ($i=1; $i <= $pages; $i++){
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
					echo ($paged == $i && empty($id_result))? "<span class='current'" . ( !empty($id_result) ? ' data-result="' . $id_result . '"' : '' ) . " data-page='" . ($i) . "'>".$i."</span>":"<a href='".get_pagenum_link($i)."'" . ( !empty($id_result) ? ' data-result="' . $id_result . '"' : '' ) . " data-page='" . ($i) . "' class='page larger'>".$i."</a>";
				}
			}
			if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'" . ( !empty($id_result) ? ' data-result="' . $id_result . '"' : '' ) . " data-page='" . ($paged + 1) . "' class='page larger'>›</a>";
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'" . ( !empty($id_result) ? ' data-result="' . $id_result . '"' : '' ) . " data-page='" . ($pages) . "' class='page larger'>»</a>";
			echo "</div>";
		}

	}

endif;

/** ====== END sky_pagination ====== **/

/* -------------------------------------------------------
 * Create functions sky_the_tag
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_the_tag' ) ) :
	
	function sky_the_tag( $post_ID = null ) {

		if ( empty( $post_ID ) ) $post_ID = get_the_ID();

		$game_tags = wp_get_post_terms( $post_ID, sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-tags' ) );

		if ( $game_tags ) :

			echo "<div class='sky_tag'>";

			foreach ($game_tags as $tags) :
				
				$term_link = get_term_link( $tags );

				echo '<a target="_blank" href="' . esc_url( $term_link ) . '" title="' . $tags->name . '">' . $tags->name . '</a>,';
			
			endforeach;

			echo "</div>";
		
		endif;

	}

endif;

/** ====== END sky_the_tag ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_info_other
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_info_other' ) ) :
	
	function sky_get_info_other( $title = null, $post_ID = null ) {

		if ( empty( $post_ID ) ) $post_ID = get_the_ID();

		if ( !empty( $title ) ) echo '<div class="sky_info_other">' . $title . '</div><div class="sky_info_other_entry">';

			echo "<div class='sky_info_item'>" . sprintf(
				esc_html_x( 'Đăng bởi %s', 'post author', 'sky-game' ), '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>' ) . "</div>";
			echo "<div class='sky_info_item'>" . sprintf( esc_html__( 'Nhà cung cấp: %s', 'sky-game' ), sky_get_list_offered() ) . "</div>";
			echo "<div class='sky_info_item'>" . sprintf( esc_html__( 'Cập nhật: %s', 'sky-game' ), get_the_modified_time( 'd-m-Y' ) ) . "</div>";
			
			// --- Check version
				if ( sky_get_post_meta( $post_ID, 'sky_version' ) )
					echo "<div class='sky_info_item'>" . sprintf( esc_html__( 'Phiên bản: %s', 'sky-game' ), sky_get_post_meta( $post_ID, 'sky_version' ) ) . "</div>";

			// --- Check size
				if ( sky_get_post_meta( $post_ID, 'sky_size' ) )
					echo "<div class='sky_info_item'>" . sprintf( esc_html__( 'Dung lượng: %s', 'sky-game' ), sky_get_post_meta( $post_ID, 'sky_size' ) ) . "</div>";

			// --- Check require
				if ( sky_get_post_meta( $post_ID, 'sky_requires_android' ) )
					echo "<div class='sky_info_item'>" . sprintf( esc_html__( 'Yều cầu Android: %s', 'sky-game' ), sky_get_post_meta( $post_ID, 'sky_requires_android' ) ) . "</div>";

				if ( sky_get_post_meta( $post_ID, 'sky_requires_ios' ) )
					echo "<div class='sky_info_item'>" . sprintf( esc_html__( 'Yêu cầu IOS: %s', 'sky-game' ), sky_get_post_meta( $post_ID, 'sky_requires_ios' ) ) . "</div>";

		if ( !empty( $title ) ) echo '</div>';

	}

endif;

/** ====== END sky_get_info_other ====== **/


/* -------------------------------------------------------
 * Create functions sky_add_social
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_add_social' ) ) :
	
	function sky_add_social( ) {
		global $post;
			$url = get_permalink( );
			if ( !empty(sky_get_post_meta( get_the_ID(), 'sky_description' ) ) ) :
				$description = sky_get_post_meta( get_the_ID(), 'sky_description' );
			else :
				$description = get_the_excerpt( );
			endif;
			?>
			<div class="sky-social">
				<div class="fb-like" data-layout="button_count" data-action="like" data-share="true" data-show-faces="false"></div>
				<div class="g-plusone" data-size="medium"></div>

			</div>
		<?php

	}

endif;

/** ====== END sky_add_social ====== **/



/* -------------------------------------------------------
 * Create functions sky_search_form
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_search_form' ) ) :
	
	function sky_search_form( $form ) {
		$form = '
			<form method="get" id="sky-search-form" class="sky-search-form" action="' . home_url( '/' ) . '" >
				<div>
					<input type="search" name="s" id="sky-input-search" value="' . get_search_query() . '" placeholder="' . esc_html__( 'Enter your text search', 'sky-game' ) . '">
					<input type="submit" id="sky-search-submit" value="'. esc_attr__( 'Search' ) .'" />
				</div>
			</form>';

		return $form;
	}

	add_filter( 'get_search_form', 'sky_search_form' );

endif;

/** ====== END sky_search_form ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_post_nav
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_post_nav' ) ) :
	
	function sky_get_post_nav( $post_type, $term ) {
		global $post;

		// === <<< get_posts in same custom taxonomy
		$postlist_args = array(
			'posts_per_page'       => -1,
			'orderby'              => 'menu_order title',
			'order'                => 'ASC',
			'post_type'            => $post_type,
			'your_custom_taxonomy' => $term
		); 
		$postlist = get_posts( $postlist_args );

		// === <<< get ids of posts retrieved from get_posts
			$ids = array();

			foreach ($postlist as $thepost) :
			   $ids[] = $thepost->ID;
			endforeach;

		// === <<< get and echo previous and next post in the same taxonomy        
			
			$thisindex = array_search($post->ID, $ids);
			
			$previd = isset($ids[$thisindex-1]) ? $ids[$thisindex-1] : '';
			$nextid = isset($ids[$thisindex+1]) ? $ids[$thisindex+1] : '';
			if ( !empty($previd) || !empty($nextid) ) :

				echo "<ul class=\"sky_post_nav\">";

				if ( !empty($previd) ) :
				   ?>
						<li class="sky-xs-12 sky-sm-5 pre">
							<span><?php esc_html_e( 'Bài trước', 'sky-game' ); ?></span>
							<a rel="prev" href="<?php echo get_permalink($previd) ?>" title="<?php echo get_the_title($previd) ?>"><?php echo get_the_title($previd) ?></a>
						</li>
					<?php
				endif;

				if ( !empty($nextid) ) :
					?>
						<li class="sky-xs-12 sky-sm-5 sky-sm-offset-2 next">
							<span><?php esc_html_e( 'Bài sau', 'sky-game' ); ?></span>
							<a rel="prev" href="<?php echo get_permalink($nextid) ?>" title="<?php echo get_the_title($nextid) ?>"><?php echo get_the_title($nextid) ?></a>
						</li>
					<?php
				endif;

				echo "</ul>";

			endif;

	}

endif;

/** ====== END sky_get_post_nav ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_count_social
	 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_count_social'   ) ) :
	
	function sky_get_count_social( $url = null, $social = null, $show = 'share' ) {

		if ( isset( $url ) && isset( $social ) ) :

			if ( $social == 'twitter' ) :

				$url = "http://cdn.api.twitter.com/1/urls/count.json?url={$url}";
				$content =  json_decode( file_get_contents( $url ), true );	
				return $content['count'];
			
			elseif ( $social == 'facebook' ) :
			
				$url = "https://graph.facebook.com/fql?q=SELECT%20like_count,%20total_count,%20share_count,%20click_count,%20comment_count%20FROM%20link_stat%20WHERE%20url%20=%20%22{$url}%22";
				$content = json_decode( file_get_contents( $url ), true );
				return $content['data'][0]['total_count'];
			
			elseif ( $social == 'pinterest' ) :
			
				$url = "http://api.pinterest.com/v1/urls/count.json?callback=&url={$url}";
				$content = json_decode( file_get_contents( $url ), true );
				print_r( $content );

			elseif ( $social == 'linkedin' ) :
			
				$url = "https://www.linkedin.com/countserv/count/share?url={$url}&format=json&callback=myCallback";
				$content = json_decode( file_get_contents( $url ), true);
				return $content['count'];
			
			elseif ( $social == 'stumbleupon' ) :
			
				$url = "http://www.stumbleupon.com/services/1.01/badge.getinfo?url={$url}";
				$content = json_decode( file_get_contents( $url ), true);
				return $content['result']['views'];
			
			elseif ( $social == 'google' ) :
			
				$url = sky_get_info_google( $url );
				$content = json_decode( $url );
				return $content->plus_count;
			
			endif;

			return $content;

		else :
			return;
		endif;

	}

endif;

/** ====== END sky_get_count_social   ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_info_google
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_info_google' ) ) :
	
	function sky_get_info_google( $url ) {
		// header('Cache-Control: no-cache, must-revalidate');
		// header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		// header('Content-type: application/json; charset=utf-8');
		if (isset($_GET['url'])) {
		    $url = $_GET['url'];
		    if ('/' != substr($url, -1))
		        $url = $url . '/';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$curl_results = curl_exec($ch);
		curl_close($ch);
		$json = json_decode($curl_results, true);
		$count = intval($json[0]['result']['metadata']['globalCounts']['count']);
		$data = array();
		$data['plus_count'] = (string) $count;
		$data['url'] = $url;
		return json_encode($data);
	}

endif;

/** ====== END sky_get_info_google ====== **/


/* -------------------------------------------------------
 * Create functions sky_live_search
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_live_search' ) ) :
	
	function sky_live_search() {

		$title = $_POST['title'];
        global $wpdb;
        if( isset($title) && !empty($title)):
            $sql = "SELECT id, post_title FROM $wpdb->posts  WHERE post_title LIKE '%$title%'  AND post_status ='publish' AND post_type = 'sky-game' LIMIT 13";
       		$query = $wpdb->get_results( $sql, OBJECT );
        else: exit;
        endif;
        if( isset($query) && !empty($query) ):
            echo '<ul>';
            foreach($query as $value):
                ?>
                    <li class="clearfix">
                        <img src="<?php echo sky_get_thumb( $value->id ); ?>" alt="">
                       	<div class="ajax_p_content">
                            <a href="<?php echo get_permalink( $value->id ); ?>">
                                <?php echo esc_html($value->post_title); ?>
                            </a>
                        </div>
                    </li>
                <?php
            endforeach;
            echo '</ul>';
        endif;
        wp_die();

	}

endif;

/** ====== END sky_live_search ====== **/

/* -------------------------------------------------------
 * Create functions sky_widget_show_post
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_widget_show_post' ) ) :
	
	function sky_widget_show_post( $posts_number = 5, $show = 'blog', $thumb = true ) {
		
		global $post;
		$sky_post = $post;

		$args = array( 'posts_per_page' => $posts_number );

		if ( $show == 'game' ) :

			$args['post_type'] = 'sky-game';

		endif;

		// === Order
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = '_sky_views_count';
			
		$wp_query = new WP_Query( $args );

		if ( $wp_query->have_posts() ):
			while ( $wp_query->have_posts() ) : $wp_query->the_post() ?>
			<li>
				<div class="sky-post-thumbnail">
					<a href="<?php the_permalink(); ?>" title="<?php the_title();?>" rel="bookmark">
						<img src="<?php echo sky_get_thumb( get_the_id() ); ?>" alt="<?php the_title();?>" />
					</a>
				</div>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
				<?php if ( $show == 'game' ) : ?>
					<p>
						<?php esc_html_e( 'Hỗ trợ', 'sky-game' ); ?>&nbsp;
						<?php sky_get_list_support_os(); ?>
					</p>
				<?php else : ?>
					<span class="sky-date"><i class="icon_clock_alt"></i><?php echo get_the_date(); ?></span>
				<?php endif; ?>
			</li>
			<?php endwhile;
		endif;
		
		$post = $sky_post;
		wp_reset_query();

	}

endif;

/** ====== END sky_widget_show_post ====== **/