<?php
/**
 * Style Functions for SKY Framework.
 * This file contains functions for calculating style (normally it's css class) base on settings from admin side.
 *
 * @package    SKY Framework
 * @version    1.0.0
 */

if (!function_exists('sky_body_class')):
	function sky_body_class($output) {
		global $wp_customize;
		if (isset($wp_customize)) {
			$output[] = 'is-customize-preview';
		}

		// Preload
		if( sky_get_option( 'sky_preload', false ) ) {
			$output[] = 'enable-preload';
		}

		$page_layout = get_page_layout();
		if ($page_layout == 'fullwidth') {
			$output[] = ' page-fullwidth';
		} elseif ($page_layout == 'left_sidebar') {
			$output[] = ' page-left-sidebar';
		} else {
			$output[] = ' page-right-sidebar';
		}
		
		switch (sky_get_option('sky_site_layout', 'fullwidth')) {
			case 'boxed':
				// if(get_page_template_slug() != 'page-full-width.php')
				$output[] = 'boxed-layout';
			break;
			default:
				$output[] = 'full-width-layout';
			break;
		}

        if ( sky_get_option('sky_layout_rtl','no') == 'yes' ){
            $output[] = 'theme-rtl';
        }
		
		return $output;
	}
endif;
add_filter('body_class', 'sky_body_class');

if (!function_exists('sky_header_class')):
	function sky_header_class() {
		$class = '';
		$navbar_position = sky_get_option('sky_header_nav_position', 'fixed_top');
        $transparent = sky_get_post_meta(get_the_ID(),'_sky_wp_page_menu_transparent');
		if ($navbar_position == 'fixed_top') {
			$class = 'fixed_top';
		}
        if( is_page() ){
            if($transparent){
                $class .= ' header_transparent';
            }
        }

		echo $class;
	}
endif;


if (!function_exists('sky_main_class')):
	function sky_main_class() {
		$class = 'sky-main';
		$page_layout = get_page_layout();

		if ($page_layout == 'fullwidth') {
			$class.= ' sky-md-12';
		} elseif ($page_layout == 'left_sidebar') {
			$class.= ' sky-md-9 pull-right';
		} else {
			$class.= ' sky-md-9';
		}
		
		echo $class;
	}
endif;

if (!function_exists('sky_sidebar_class')):
	function sky_sidebar_class() {
		$class = ' sky-sidebar sky-xs-12 sky-md-4';
		$page_layout = get_page_layout();
		
		if ( $page_layout == 'left_sidebar' ) {
			$class .= ' sky-sidebar-left pull-left';
		}
		
		echo $class;
	}
endif;

if (!function_exists('sky_post_class')):
	function sky_post_class($output) {
		if (has_featured_content()) {
			$output[] = 'has-featured';
		} else {
			$output[] = 'no-featured';
		}
		
		return $output;
	}
	
	// add_filter('post_class', 'sky_post_class');
endif;

if (!function_exists('sky_page_class')):
	function sky_page_class() {
		$class = ' sky-page';
		
		echo $class;
	}
endif;