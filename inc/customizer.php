<?php
// /**
//  * Sky Game Theme Customizer.
//  *
//  * @package Sky Game
//  */

// /**
//  * Add postMessage support for site title and description for the Theme Customizer.
//  *
//  * @param WP_Customize_Manager $wp_customize Theme Customizer object.
//  */
// function sky_game_customize_register( $wp_customize ) {
// 	/*
// 	 * https://codex.wordpress.org/Plugin_API/Action_Reference/customize_register
// 	 * The 'customize_register' action hook is used to customize and manipulate the Theme Customization admin screen introduced in WordPress Version 3.4. 
// 	 * This hook is a component of the Theme Customization API.
// 	 */
// 	// $helper = new NOO_Customizer_Helper( $wp_customize );
// 	// -- [ Remove section default ] -- //
// 		$wp_customize->remove_section( 'title_tagline');
// 		$wp_customize->remove_section( 'static_front_page');
// 		$wp_customize->remove_section( 'colors');
// 		$wp_customize->remove_section( 'background_image');
// 		$wp_customize->remove_section( 'header_image');

// 	// -- [ Register custom general ] -- //
// 		$wp_customize->add_section('sky_generel', array(
// 	        'title'    => esc_html__('Sky General', 'sky-game'),
// 	        'description' => '',
// 	        'priority' => 25,
// 	    ));

// 	    /*
// 	     * Add setting background header
// 	     */
// 		    $wp_customize->add_setting('sky_generel[logo_watermark]', array(
// 		        'default'        => '',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo_watermark', array(
// 		        'label'    => esc_html__('Logo Watermark', 'sky-game'),
// 		        'section'  => 'sky_generel',
// 		        'settings' => 'sky_generel[logo_watermark]',
// 		    )));

// 	// -- [ Register custom header ] -- //
// 		$wp_customize->add_section('sky_header', array(
// 	        'title'    => esc_html__('Sky Header', 'sky-game'),
// 	        'description' => '',
// 	        'priority' => 30,
// 	    ));

// 	    /*
// 	     * Add setting background header
// 	     */
// 		    $wp_customize->add_setting('sky_header[bg]', array(
// 		        'default'        => '',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'bg', array(
// 		        'label'    => esc_html__('Header Background Image', 'sky-game'),
// 		        'section'  => 'sky_header',
// 		        'settings' => 'sky_header[bg]',
// 		    )));

// 	    /*
// 	     * Add setting logo
// 	     */
	    	
// 		    $wp_customize->add_setting('sky_header[logo]', array(
// 		        'default'        => SKY_THEME_ASSETS_URI . '/images/logo.svg',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo', array(
// 		        'label'    => esc_html__('Logo', 'sky-game'),
// 		        'section'  => 'sky_header',
// 		        'settings' => 'sky_header[logo]',
// 		    )));

// 	// -- [ Register custom footer ]
// 		$wp_customize->add_section('sky_footer', array(
// 	        'title'    => esc_html__('Sky Footer', 'sky-game'),
// 	        'description' => '',
// 	        'priority' => 35,
// 	    ));

// 	    /*
// 	     * Add setting column footer
// 	     */
// 		    $wp_customize->add_setting('sky_footer[column]', array(
// 		        'default'        => '3',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( 'column', array(
// 		        'label'    => esc_html__('Footer Columns (Widgetized)', 'sky-game'),
// 		        'section'  => 'sky_footer',
// 		        'settings' => 'sky_footer[column]',
// 		        'type'       => 'select',
// 		        'choices'    => array(
// 		        	'0' => esc_html__( 'None (No Footer Main Content)', 'sky-game' ),
// 		            '1' => esc_html__( 'One', 'sky-game' ),
// 		            '2' => esc_html__( 'Two', 'sky-game' ),
// 		            '3' => esc_html__( 'Three', 'sky-game' ),
// 		            '4' => esc_html__( 'Four', 'sky-game' ),
// 		        )
// 		    ));

// 		/*
// 	     * Add setting background column footer
// 	     */
// 		    $wp_customize->add_setting('sky_footer[f_bg_column]', array(
// 		        'default'        => '',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'f_bg_column', array(
// 		        'label'    => esc_html__('Footer Column Background Image', 'sky-game'),
// 		        'section'  => 'sky_footer',
// 		        'settings' => 'sky_footer[f_bg_column]',
// 		    )));

// 		/*
// 	     * Add setting background footer
// 	     */
// 		    $wp_customize->add_setting('sky_footer[f_bg]', array(
// 		        'default'        => '',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'f_bg', array(
// 		        'label'    => esc_html__('Footer Background Image', 'sky-game'),
// 		        'section'  => 'sky_footer',
// 		        'settings' => 'sky_footer[f_bg]',
// 		    )));

// 		/*
// 	     * Add setting logo footer
// 	     */
// 		    $wp_customize->add_setting('sky_footer[f_logo]', array(
// 		        'default'        => '',
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'f_logo', array(
// 		        'label'    => esc_html__('Upload Footer Logo', 'sky-game'),
// 		        'section'  => 'sky_footer',
// 		        'settings' => 'sky_footer[f_logo]',
// 		    )));

// 		/*
// 	     * Add setting bottom bar content footer
// 	     */
// 		    $wp_customize->add_setting('sky_footer[f_content]', array(
// 		        'default'        => esc_html__( '&copy; Copyright 2015 SkyGame. Designed with by SkyTheme', 'sky-game'),
// 		        'capability'     => 'edit_theme_options',
// 		        'type'           => 'option',
		 
// 		    ));
// 		    $wp_customize->add_control( 'f_content', array(
// 		        'label'    => esc_html__('Bottom Bar Content (HTML)', 'sky-game'),
// 		        'section'  => 'sky_footer',
// 		        'settings' => 'sky_footer[f_content]',
// 		        'type'       => 'textarea',
// 		    ));
    
// }
// add_action( 'customize_register', 'sky_game_customize_register' );

// /**
//  * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
//  */
// function sky_game_customize_preview_js() {
// 	wp_enqueue_script( 'sky_game_customizer', SKY_THEME_ASSETS_URI . '/js/min/customizer.min.js', array( 'customize-preview' ), '20130508', true );
// }
// add_action( 'customize_preview_init', 'sky_game_customize_preview_js' );


// //
// // Register NOO Customizer Sections and Options
// //

// // 3. Typography options.
// if ( ! function_exists( 'noo_customizer_register_options_typo' ) ) :
// 	function noo_customizer_register_options_typo( $wp_customize ) {

// 		// declare helper object.
// 		$helper = new NOO_Customizer_Helper( $wp_customize );

// 		// Section: Typography
// 		$helper->add_section(
// 			'noo_customizer_section_typo',
// 			esc_html__( 'Typography', 'sky-game' ),
// 			esc_html__( 'Customize your Typography settings. Merito integrated all Google Fonts. See font preview at <a target="_blank" href="http://www.google.com/fonts/">Google Fonts</a>.', 'sky-game' )
// 		);

// 		// Control: Use Custom Fonts
// 		$helper->add_control(
// 			'noo_typo_use_custom_fonts',
// 			'noo_switch',
// 			esc_html__( 'Use Custom Fonts?', 'sky-game' ),
// 			0,
// 			array( 'json' => array( 
// 				'on_child_options'  => 'noo_typo_headings_font,noo_typo_body_font' 
// 				)
// 			),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Control: Use Custom Font Color
// 		$helper->add_control(
// 			'noo_typo_use_custom_fonts_color',
// 			'noo_switch',
// 			esc_html__( 'Custom Font Color?', 'sky-game' ),
// 			0,
// 			array( 'json' => array(
// 				'on_child_options'  => 'noo_typo_headings_font_color,noo_typo_body_font_color'
// 				)
// 			),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Sub-Section: Headings
// 		$helper->add_sub_section(
// 			'noo_typo_sub_section_headings',
// 			esc_html__( 'Headings', 'sky-game' )
// 		);

// 		// Control: Headings font
// 		$helper->add_control(
// 			'noo_typo_headings_font',
// 			'google_fonts',
// 			esc_html__( 'Headings Font', 'sky-game' ),
// 			'',
// 			array(),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Control: Headings Font Color
// 		$helper->add_control(
// 			'noo_typo_headings_font_color',
// 			'color_control',
// 			esc_html__( 'Font Color', 'sky-game' ),
// 			'',
// 			array(),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Control: Headings Font Uppercase
// 		$helper->add_control(
// 			'noo_typo_headings_uppercase',
// 			'checkbox',
// 			esc_html__( 'Transform to Uppercase', 'sky-game' ),
// 			0,
// 			array(),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Sub-Section: Body
// 		$helper->add_sub_section(
// 			'noo_typo_sub_section_body',
// 			esc_html__( 'Body', 'sky-game' )
// 		);

// 		// Control: Body font
// 		$helper->add_control(
// 			'noo_typo_body_font',
// 			'google_fonts',
// 			esc_html__( 'Body Font', 'sky-game' ),
// 			'',
// 			array(),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Control: Body Font Size
// 		$helper->add_control(
// 			'noo_typo_body_font_size',
// 			'font_size',
// 			esc_html__( 'Font Size (px)', 'sky-game' ),
// 			'',
// 			array(),
// 			array( 'transport' => 'postMessage' )
// 		);

// 		// Control: Body Font Color
// 		$helper->add_control(
// 			'noo_typo_body_font_color',
// 			'color_control',
// 			esc_html__( 'Font Color', 'sky-game' ),
// 			'',
// 			array(),
// 			array( 'transport' => 'postMessage' )
// 		);
// 	}
// // add_action( 'customize_register', 'noo_customizer_register_options_typo' );
// endif;