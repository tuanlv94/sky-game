<?php 
// ===== <<< [ Checking ABSPATH root ] >>> ===== //
	defined('ABSPATH') || ( header( "HTTP/1.1 403 Forbidden" ) & die( "403.14 - Directory listing denied." ) );

if( !class_exists( 'Sky_Spinner_Setting' ) ) :

	class Sky_Spinner_Setting {

		public function __construct() {

			/*
			 * Load action, filter
			 */
				add_action('admin_init', array(&$this,'admin_init'));
				add_action('sky_setting_sky_spinner_settings', array( &$this, 'setting_page' ));
				add_filter('sky_settings_tabs_array', array( &$this, 'add_seting_spinner_tab' ));

				add_action( 'wp_ajax_sky_spinner', array( &$this, 'sky_spinner_ajax' ) );

				add_action( 'add_meta_boxes', array( &$this, 'sky_spinner_create_meta_box' ) );

				add_filter( 'mce_css', array( &$this, 'sky_spinner_mce_css' ) );

		}

		public function admin_init(){

			register_setting( 'sky_spinner_settings' , 'sky_spinner_settings' );

		}

		public function add_seting_spinner_tab( $tabs ) {

			$tabs['sky_spinner_settings'] = esc_html__(  'Spinner', 'sky-game' );
			return $tabs;

		}

		public static function get_setting($id = null, $default = null){

			$sky_spinner_settings = get_option('sky_spinner_settings');

			if(isset($sky_spinner_settings[$id]))
				return $sky_spinner_settings[$id];

			return $default;

		}

		public function sky_spinner_create_meta_box() {
			
			$screens = array( 'post', 'sky-game' );

			foreach ( $screens as $screen ) :
				
				add_meta_box( 'sky_spinner-meta-boxes', esc_html__(  'Sky Spinner', 'sky-game' ), array( &$this, 'sky_spinner_metabox' ), $screen , 'normal', 'high' );

			endforeach;
			
		}

		public function sky_spinner_mce_css( $mce_css ) {
			if ( ! empty( $mce_css ) )
				$mce_css .= ',';

			$mce_css .= SKY_THEME_ASSETS_URI . '/css/sky-editor.css';

			return $mce_css;
		}

		public function sky_spinner_ajax() {

			// ===== <<< [ checking ajax ] >>> ===== //
				check_ajax_referer( 'sky-script-admin', 'security' );

			// ===== <<< [ Get var ] >>> ===== //
				$id = $_POST['id'];


			if ( $_POST['process'] == 'delete' ) :

				$delete = get_option('sky_spinner' ,array() );
				unset($delete[$id]);
				update_option('sky_spinner' , $delete );
			
			elseif ( $_POST['process'] == 'edit' ) :

				$value     = $_POST['value'];
				$edit      = get_option('sky_spinner' ,array() );
				$edit[$id] = $value;
				update_option( 'sky_spinner', $edit );

			elseif ( $_POST['process'] == 'add' ) :

				$value    = $_POST['value'];
				$add      = get_option('sky_spinner' ,array() );
				$add[$id] = $value;
				update_option( 'sky_spinner', $add );			

			elseif ( $_POST['process'] == 'spin' ) :

				require_once(SKY_THEME_INCLUDES_FUNCTION . '/sky-spintax.php');
				
				$spin             = new Sky_Auto_Spin( $id, $_POST['title'], $_POST['post']);
				$return           = $spin->spin_wrap();
				$return['status'] = 'success';

				print_r(json_encode($return));			

			elseif ( $_POST['process'] == 're_generate' ) :

				require_once(SKY_THEME_INCLUDES_FUNCTION . '/sky-spintax.php');
	
				//generate new spintax
					$content        = stripslashes( $_POST['title'].'**9999**'.$_POST['post']);
					$spintax        = new Sky_Spinner_Spintax;
					$spintaxed      = $spintax->spin( $content );
					$spintaxed_arr  = explode( '**9999**', $spintaxed );
					$spintaxed_ttl  = $spintaxed_arr[0];
					$spintaxed_cnt  = $spintaxed_arr[1];
					$spintaxed2     = $spintax->editor_form;
					$spintaxed2_arr = explode( '**9999**', $spintaxed2 );
					$spintaxed2_cnt = $spintaxed2_arr[1];
				
				
				//update post meta
					update_post_meta($id, 'spintaxed_cnt2', $spintaxed2_cnt);
					update_post_meta($id, 'spintaxed_ttl', $spintaxed_ttl);
					update_post_meta($id, 'spintaxed_cnt', $spintaxed_cnt);
				
				$return['spintaxed_ttl']  = $spintaxed_arr[0];
				$return['spintaxed_cnt2'] = $spintaxed2_arr[1];
				$return['status']         = 'success';
				
				print_r(json_encode($return));		

			endif;

			wp_die();

		}

		public function sky_spinner_metabox( ) {
			$post_id=  get_the_ID();
			?>
			<div class="post_rewrite">
				<a id="sky-spinner-post-rewrite" title="Rewrite the post" class="button  button-primary button-large" href="#"> Rewrite the post</a>
				<img id="sky-ajax-loading1" class="ajax-loading" src=" <?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif">
			</div>

			<?php 
				if( trim( get_post_meta($post_id, 'spinned_cnt' , 1) ) !='' ) :

					$display = '';

				else :

					$display='display:none';

				endif;
			?>

			<div class="categorydiv sky-spinner-meta" id="taxonomy-category" style="<?php echo $display ?>">
	
				<ul class="category-tabs" id="category-tabs">
					<li id="sky-spin-rewritten-head" class="tab_head tabs"> Rewritten Article </li>
					<li class="tab_head hide-if-no-js"> Synonyms </li>
					<li class="tab_head hide-if-no-js"> Original Article Before Rewriting </li>
				</ul>

				<div class="tabs-panel visible-overflow"   style="display: block;">
					
					<p><i>Rewritten instance of the original article</i></p>
					
					<div class="sky-spin-form">
					
						<div id="field-sky-spin-rewritten-title-container" class="field">
							<label for="field-sky-spin-rewritten-title">
							    Rewritten Title :
							</label>
							<input value="<?php echo get_post_meta($post_id, 'spintaxed_ttl' , 1)   ?>" name="sky-spin-rewritten-title" id="field-sky-spin-rewritten-title"  type="text">
						</div>
						
						<div id="field-sky-spin-rewritten-post-container" class="field" >
							<label for="field-sky-spin-rewritten-post">
								Rewritten post :
							</label>
							
							<?php 
								
							$args = array(
								'textarea_rows' => 15,
								'teeny'         => true,
								'quicktags'     => false,
								'media_buttons' => false,
								'tinymce'       => array(
									'extended_valid_elements' => "span[id|class|title|style|synonyms]",
									'setup'     =>  "function (ed) {
												    	ed.onLoadContent.add(function(ed, o) {
												          	// Output the element name
															jQuery(\"#spinner-editor_ifr\").parent().append(
																'<ul id=\"spinner_dev\"><li>test</li></ul>'
															);
															var mouseX;
															var mouseY;
															jQuery(jQuery(\"#spinner-editor_ifr\").contents()[0], window).bind(\"mousemove\", function(e) {
																mouseX = e.pageX; 
																mouseY = e.pageY;
															});

															var currentSynonym ;
															  
															jQuery(\"#spinner-editor_ifr\").contents().find('.synonym').mouseover(function(){

																currentSynonym=this;

																//empty list
																jQuery('#spinner_dev li').remove();

																var synonyms = jQuery(this).attr('synonyms');
																synonyms_arr =synonyms.split('|') ;
																jQuery.each(synonyms_arr, function (index, value) {
																    if (value != '') {
																		jQuery('#spinner_dev').append('<li>' + value + '</li>');            
																    }
																});

	  															jQuery('#spinner_dev').css({
	  																'top' : mouseY - 13 +45 - jQuery(\"#spinner-editor_ifr\").contents().find('body').scrollTop(),
	  																'left' : mouseX -5
	  															}).fadeIn('slow');
	  															
	  															jQuery('#spinner_dev').focus();
	  
	  															jQuery('#spinner_dev li').click(function(){
	    
																    jQuery(currentSynonym).html(jQuery(this).html());
																    jQuery('#spinner_dev').hide();
																
																});
 
															});

															jQuery('#spinner_dev').mouseleave(function(){
															  	jQuery('#spinner_dev').hide();
															});


															jQuery('#spinner_dev   li').click(function(event){
															    console.log(jQuery(this).html());
															     event.stopImmediatePropagation();
															    return false;
															});
								
														           
														});
																						
													},"  
								)
							);
				
							wp_editor(  get_post_meta($post_id, 'spintaxed_cnt2', 1) , 'spinner-editor', $args );
							?>
						</div>
			
						<input type="hidden" value="yes" name="wp_auto_spinner_manual_flag" />

						<div id="form-submit" class="field" style="padding:20px 0">
						    <a id="sky-spinner-stoeditor" title="Send to editor " class="button" href="#">Send to editor</a>
						    <a  id="sky-auto-spinner-regenerate-re" title="regenerate"class="button" href="<?php echo site_url('/?wp_auto_spinner=ajax&action=rewrite'); ?>"> Generate new post</a>
						    <img id="sky-ajax-loading3" class="ajax-loading" src=" <?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif">
						</div>
		
					</div>
		
					<div class="clear"></div>
		
				</div>
	
				<div class="tabs-panel visible-overflow" style="display: none;">
					<p><i>Identified synonyms from the original article</i></p>
					
					<div class="sky-spin-form">
					
						<div id="field-sky-spin-rewritten-title-container" class="field">
							<label for="field-sky-spin-rewritten-title">
							    Title :
							</label>
							<input value="<?php echo  get_post_meta($post_id, 'spinned_ttl' , 1)  ?>" name="sky-spin-rewritten-titlesyn" id="field-sky-spin-rewritten-titlesyn"  type="text">
						</div>
						
						<div id="field-sky-spin-rewritten-post-container" class="field" >
							<label for="field-sky-spin-rewritten-post">
								post :
							</label>
							<textarea  rows="5" cols="20" name="sky-spin-rewritten-postsyn" id="field-sky-spin-rewritten-postsyn"><?php echo  get_post_meta($post_id, 'spinned_cnt' , 1)  ?></textarea>
						</div>
						
						<div id="form-submit" class="field" style="padding:20px 0"> 
						    <a  id="sky-auto-spinner-regenerate"   title="Rewrite the post " class="button  " href="<?php echo site_url('/?wp_auto_spinner=ajax&action=rewrite'); ?>"> Generate new post from above synonyms </a>
						    <img id="sky-ajax-loading2" class="ajax-loading" src=" <?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif">
						</div>
					
					</div>
					<div class="clear"></div>
					
					
				</div>
	
				<div id="category-pop" class="tabs-panel  visible-overflow" style="display: none;">
					<p>
						<i>
							Original article content before generating synonyms
						</i>
					</p>
					
					<div class="sky-spin-form">
					
						<div id="field-sky-spin-rewritten-title-container" class="field">
							<label for="field-sky-spin-rewritten-title">
							    Original Title :
							</label>
							<input value="<?php echo  get_post_meta($post_id, 'original_ttl' , 1)  ?>" name="sky-spin-rewritten-titleori" id="field-sky-spin-rewritten-titleori"  type="text">
						</div>
						
						<div id="field-sky-spin-rewritten-post-container" class="field" >
							<label for="field-sky-spin-rewritten-post">
								Original post :
							</label>
							<textarea  rows="5" cols="20" name="sky-spin-rewritten-postori" id="field-sky-spin-rewritten-postori"><?php echo  get_post_meta($post_id, 'original_cnt' , 1)  ?></textarea>
						</div>
						
						<div id="form-submit" class="field" style="padding:20px 0">
						    <a id="sky-spin-restore" title="Rewrite the post " class="button" href="#"> Restore original post </a>
						</div>
					
					</div>
					<div class="clear"></div>
					
				</div>

			</div>

			<?php

		}

		public function setting_page() {
			// =====
		    	settings_fields('sky_spinner_settings');

		    // ===== set default

	    	?>
	    		<table class="widefat sky-spinner">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Spinner Settings', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Spinner Settings', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<?php esc_html_e( 'All Words ', 'sky-game' ); ?>
                                <select name="sometext" size="20" id="word_select">
                               
                                </select>
                                <button id="edit_synonyms" title="<?php esc_html_e( 'Edit Synonym', 'sky-game' ); ?>">
                                    <i class="fa fa-pencil-square-o"></i> <?php esc_html_e( 'Edit', 'sky-game' ); ?>
                                </button>
                                
                                <button  id="save_synonyms" disabled="disabled" title="<?php esc_html_e( 'Save Synonym', 'sky-game' ); ?>">
                                    <i class="fa fa-floppy-o"></i> <?php esc_html_e( 'Save', 'sky-game' ); ?>
                                </button>
                                
                                <button  id="cancel_synonyms" disabled="disabled" title="<?php esc_html_e( 'Cancel', 'sky-game' ); ?>">
                                    <i class="fa fa-ban"></i> <?php esc_html_e( 'Cancle', 'sky-game' ); ?>
                                </button>
                                
                                <button  id="delete_synonyms" title="<?php esc_html_e( 'Delete Synonym', 'sky-game' ); ?>">
                                    <i class="fa fa-trash-o"></i> <?php esc_html_e( 'Trash', 'sky-game' ); ?>
                                </button>
							</td>
							<td>
								<p>
									<?php esc_html_e( 'Current Word', 'sky-game' ); ?>
								</p>
                                <input disabled="disabled" type="text" id="synonym_word" />
                                <p>
                                	<?php esc_html_e( 'Word synonyms', 'sky-game' ); ?>
                                </p>
                                <textarea disabled="disabled" rows="15" cols="30" name="name" id="word_synonyms"></textarea>
							</td>
						</tr>
					</tbody>
				</table>

				<table class="widefat">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Add New Synonyms', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Add New Synonyms', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<p>one word per line</p>
								<textarea  style="width:100%" cols="15" rows="3" class="mceEditor" id="content" name="content"></textarea>
								<button id="add_synonyms" class="button">Add synonyms</button>
							</td>
						</tr>
					</tbody>
				</table>

	    	<?php

	    }

	}

	new Sky_Spinner_Setting();

endif;