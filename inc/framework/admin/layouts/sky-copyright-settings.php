<?php 
// ===== <<< [ Checking ABSPATH root ] >>> ===== //
	defined('ABSPATH') || ( header( "HTTP/1.1 403 Forbidden" ) & die( "403.14 - Directory listing denied." ) );

if( !class_exists( 'Sky_Copyright_Setting' ) ) :

	class Sky_Copyright_Setting {

		public function __construct() {

			/*
			 * Load action, filter
			 */
				add_action('admin_init', array(&$this,'admin_init'));
				add_action('sky_setting_sky_copyright_settings', array( &$this, 'setting_page' ));
				add_filter('sky_settings_tabs_array', array( &$this, 'add_seting_copyright_tab' ));

		}

		public function admin_init(){

			register_setting( 'sky_copyright_settings' , 'sky_copyright_settings' );

		}

		public function add_seting_copyright_tab( $tabs ) {

			$tabs['sky_copyright_settings'] = esc_html__(  'Copyright', 'sky-game' );
			return $tabs;

		}

		public static function get_setting($id = null, $default = null){

			$sky_copyright_settings = get_option('sky_copyright_settings');

			if(isset($sky_copyright_settings[$id]))
				return $sky_copyright_settings[$id];

			return $default;

		}

		public function setting_page() {
			// =====
		    	settings_fields('sky_copyright_settings');

		    // ===== set default
		    	$url = self::get_setting('url') ? self::get_setting('url') : 'SkyGame.Mobi'; 
		    	$copyright = self::get_setting('copyright') ? self::get_setting('copyright') : 'Download game free at SkyGame.Mobi'; 
		    	$delete = self::get_setting('delete') ? self::get_setting('delete') : 'Download game free at SkyGame.Mobi';
		    	$watermark_image = self::get_setting('watermark_image') ? self::get_setting('watermark_image') : SKY_SET_THUMBNAIL_DEFAULT;
	    	
		    	if(function_exists( 'wp_enqueue_media' )) :
				
					wp_enqueue_media();
				
				else :
				
					wp_enqueue_style('thickbox');
					wp_enqueue_script('media-upload');
					wp_enqueue_script('thickbox');
				
				endif;

				if ($watermark_image) :
					$image = wp_get_attachment_url( $watermark_image );
				else :
					$image = SKY_SET_THUMBNAIL_DEFAULT;
				endif;
	    	?>
	    		<table class="widefat">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'General', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Copyright Mod Java', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<?php esc_html_e( 'URL: ', 'sky-game' ); ?>
							</td>
							<td class="help">
								<a href="#" title="" class="help_tip">[?]</a>
							</td>
							<td>
								<input type='text' name='sky_copyright_settings[url]' value='<?php echo $url; ?>' placeholder="<?php esc_html_e( 'SkyGame.Mobi', 'sky-game' ); ?>" class='regular-text' />
							</td>
						</tr>
						<tr>
							<td>
								<?php esc_html_e( 'Text Copyright: ', 'sky-game' ); ?>
							</td>
							<td class="help">
								<a href="#" title="" class="help_tip">[?]</a>
							</td>
							<td>
								<input type='text' name='sky_copyright_settings[copyright]' value='<?php echo $copyright; ?>' placeholder="<?php esc_html_e( 'Download game free at SkyGame.Mobi', 'sky-game' ); ?>" class='regular-text' />
							</td>
						</tr>
						<tr>
							<td>
								<?php esc_html_e( 'Text Delete App: ', 'sky-game' ); ?>
							</td>
							<td class="help">
								<a href="#" title="" class="help_tip">[?]</a>
							</td>
							<td>
								<input type='text' name='sky_copyright_settings[delete]' value='<?php echo $delete; ?>' placeholder="<?php esc_html_e( 'Download game free at SkyGame.Mobi', 'sky-game' ); ?>" class='regular-text' />
							</td>
						</tr>
					</tbody>
				</table>

				<table class="widefat">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'General', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Watermark', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<div id="watermark_thumbnail"><img src="<?php echo $image; ?>" /></div>
								<input type="hidden" id="watermark_image" name="sky_copyright_settings[watermark_image]" value="<?php echo $image; ?>" />
							</td>
							<td class="help">
								<a href="#" title="" class="help_tip">[?]</a>
							</td>
							<td>
								<button type="button" class="upload_image_button button"><?php esc_html_e('Upload/Add image', 'sky-game'); ?></button>
								<button type="button" class="remove_image_button button"><?php esc_html_e('Remove image', 'sky-game'); ?></button>
							</td>
							<script type="text/javascript">
				
								jQuery(function(){
				
									 // Only show the "remove image" button when needed
									 if ( ! jQuery('#watermark_image').val() )
										 jQuery('.remove_image_button').hide();
				
									// Uploading files
									var file_frame;
				
									jQuery(document).on( 'click', '.upload_image_button', function( event ){
				
										event.preventDefault();
				
										// If the media frame already exists, reopen it.
										if ( file_frame ) {
											file_frame.open();
											return;
										}
				
										// Create the media frame.
										file_frame = wp.media.frames.downloadable_file = wp.media({
											title: "<?php esc_html_e( 'Choose an image', 'sky-game' ); ?>",
											button: {
												text: "<?php esc_html_e( 'Use image', 'sky-game' ); ?>",
											},
											multiple: false
										});
				
										// When an image is selected, run a callback.
										file_frame.on( 'select', function() {
											attachment = file_frame.state().get('selection').first().toJSON();
				
											jQuery('#watermark_image').val( attachment.id );
											jQuery('#watermark_thumbnail img').attr('src', attachment.url );
											jQuery('.remove_image_button').show();
										});
				
										// Finally, open the modal.
										file_frame.open();
									});
				
									jQuery(document).on( 'click', '.remove_image_button', function( event ){
										jQuery('#watermark_thumbnail img').attr('src', '<?php echo SKY_SET_THUMBNAIL_DEFAULT; ?>');
										jQuery('#watermark_image').val('');
										jQuery('.remove_image_button').hide();
										return false;
									});
								});
				
							</script>
							<div class="clear"></div>
						</tr>
					</tbody>
				</table>
	    		
	    	<?php

	    }

	}

	new Sky_Copyright_Setting();

endif;