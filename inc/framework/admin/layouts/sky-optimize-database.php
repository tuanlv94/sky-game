<?php 
// ===== <<< [ Checking ABSPATH root ] >>> ===== //
	defined('ABSPATH') || ( header( "HTTP/1.1 403 Forbidden" ) & die( "403.14 - Directory listing denied." ) );

if( !class_exists( 'Sky_Optimize_Database_Setting' ) ) :

	class Sky_Optimize_Database_Setting {

		public function __construct() {

			/*
			 * Load action, filter
			 */
				add_action('admin_init', array(&$this,'admin_init'));
				add_action('sky_setting_sky_optimize_database_settings', array( &$this, 'setting_page' ));
				add_filter('sky_settings_tabs_array', array( &$this, 'add_seting_optimize_database_tab' ));

		}

		public function admin_init(){

			register_setting( 'sky_optimize_database_settings' , 'sky_optimize_database_settings' );

		}

		public function add_seting_optimize_database_tab( $tabs ) {

			$tabs['sky_optimize_database_settings'] = esc_html__(  'Optimize Database', 'sky-game' );
			return $tabs;

		}

		public static function get_setting($id = null, $default = null){

			$sky_optimize_database_settings = get_option('sky_optimize_database_settings');

			if(isset($sky_optimize_database_settings[$id]))
				return $sky_optimize_database_settings[$id];

			return $default;

		}

		public function setting_page() {
			// =====
		    	settings_fields('sky_optimize_database_settings');
		    	$sky_optimize = new Sky_Database();
		    // ===== set default
		    	$sky_count_unused = $sky_optimize->sky_cleaner_count();
		    	$url = self::get_setting('url') ? self::get_setting('url') : 'SkyGame.Mobi';
	    	?>
	    		<table class="widefat">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Clean Database', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Clean Database', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr class="sky-menu">
							<td>
								<?php esc_html_e( 'Elements to clean', 'sky-game' ); ?>
							</td>
							<td class="help">
								<?php esc_html_e( 'Count', 'sky-game' ); ?>
							</td>
							<td>
								<?php esc_html_e( 'Select', 'sky-game' ); ?>
							</td>
						</tr>

						<?php 
							if( $sky_count_unused["total"] == 0 ) : ?>

								<tr class="">
									<td class="column-name" style="color: #999">
										<?php esc_html_e( 'Nothing to do!' , 'sky-game' ); ?>
									</td>
									<td class="column-name"></td>
									<td class="column-name"></td>
								</tr>

							<?php else : ?>
						
							<?php if( $sky_count_unused["revision"] > 0 ) : ?>

									<tr class="">
										<td class="column-name">
											<?php esc_html_e( 'Revision',   'sky-game' ); ?>
										</td>
										<td class="column-name skyCount">
											<?php echo $sky_count_unused["revision"]; ?>
										</td>
										<td class="column-name">
											<div class="squaredFour">
												<input id="sky_check1" type="checkbox" name="sky_post_type[]" value="revision" />
												<label for="sky_check1"></label>
											</div>
										</td>
									</tr>

							<?php endif; ?>
								
							<?php if( $sky_count_unused["draft"] > 0 ) : ?>
								
								<tr class="">
									<td class="column-name">
										<?php esc_html_e( 'Draft' , 'sky-game' ); ?>
									</td>
									<td class="column-name skyCount">
										<?php echo $sky_count_unused["draft"]; ?>
									</td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check2" type="checkbox" name="sky_post_type[]" value="draft" />
											<label for="sky_check2"></label>
										</div>
									</td>
								</tr>
							
							<?php endif; ?>
						
							<?php if( $sky_count_unused["autodraft"] > 0 ) : ?>

								<tr class="">
									<td class="column-name">
										<?php esc_html_e( 'Auto Draft' , 'sky-game' ); ?>
									</td>
									<td class="column-name skyCount">
										<?php echo $sky_count_unused["autodraft"]; ?>
									</td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check3" type="checkbox" name="sky_post_type[]" value="autodraft" />
											<label for="sky_check3"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>				
							
							<?php if( $sky_count_unused["moderated"] > 0 ) : ?>

								<tr class="">
									<td class="column-name">
										<?php esc_html_e( 'Moderated Comments' , 'sky-game' ); ?>
									</td>
									<td class="column-name skyCount">
										<?php echo $sky_count_unused["moderated"]; ?>
									</td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check4" type="checkbox" name="sky_post_type[]" value="moderated" />
											<label for="sky_check4"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>
						
							<?php if( $sky_count_unused["spam"] > 0 ) : ?>

								<tr class="">
									<td class="column-name">
										<?php esc_html_e( 'Spam Comments' , 'sky-game' ); ?>
									</td>
									<td class="column-name skyCount">
										<?php echo $sky_count_unused["spam"]; ?>
									</td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check5" type="checkbox" name="sky_post_type[]" value="spam" />
											<label for="sky_check5"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>
							
							<?php if( $sky_count_unused["trash"] > 0 ) : ?>

								<tr class="">
									<td class="column-name">
										<?php esc_html_e( 'Trash Comments' , 'sky-game' ); ?>
									</td>
									<td class="column-name skyCount">
										<?php echo $sky_count_unused["trash"]; ?>
									</td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check6" type="checkbox" name="sky_post_type[]" value="trash" />
											<label for="sky_check6"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>

							<?php if( $sky_count_unused["postmeta"] > 0 ) : ?>

								<tr class="">
									<td class="column-name"><?php esc_html_e( 'Orphan Postmeta' , 'sky-game' ); ?></td>
									<td class="column-name skyCount"><?php echo $sky_count_unused["postmeta"]; ?></td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check7" type="checkbox" name="sky_post_type[]" value="postmeta" />
											<label for="sky_check7"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>

							<?php if( $sky_count_unused["commentmeta"] > 0 ) : ?>

								<tr class="">
									<td class="column-name"><?php esc_html_e( 'Orphan Commentmeta' , 'sky-game' ); ?></td>
									<td class="column-name skyCount"><?php echo $sky_count_unused["commentmeta"]; ?></td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check8" type="checkbox" name="sky_post_type[]" value="commentmeta" />
											<label for="sky_check8"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>

							<?php if( $sky_count_unused["relationships"] > 0 ) : ?>
								
								<tr class="">
									<td class="column-name"><?php esc_html_e( 'Orphan Relationships' , 'sky-game' ); ?></td>
									<td class="column-name skyCount"><?php echo $sky_count_unused["relationships"]; ?></td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check9" type="checkbox" name="sky_post_type[]" value="relationships" />
											<label for="sky_check9"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>
							
							<?php if( $sky_count_unused["feed"] > 0 ) : ?>

								<tr class="">
									<td class="column-name"><?php esc_html_e( 'Dashboard Transient Feed' , 'sky-game' ); ?></td>
									<td class="column-name skyCount"><?php echo $sky_count_unused["feed"]; ?></td>
									<td class="column-name">
										<div class="squaredFour">
											<input id="sky_check10" type="checkbox" name="sky_post_type[]" value="feed" />
											<label for="sky_check10"></label>
										</div>
									</td>
								</tr>

							<?php endif; ?>
						
						<?php endif; ?>

						<tr>
							<th scope="col" style="font-size:13px; font-weight:bold; color:#444; border-top: 0px">
								<?php esc_html_e( 'Total', 'sky-game' ); ?>
							</th>
							<th scope="col" class="skyCount" style="border-top: 0px; font-weight:bold; color:#444;">
								<b><?php echo $sky_count_unused["total"]; ?></b>
							</th>
							<th scope="col" style="border-top: 0px"></th>
						</tr>
						
					</tbody>

				</table>
	    		
	    	<?php

	    }

	}

	new Sky_Optimize_Database_Setting();

endif;