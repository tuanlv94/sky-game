<?php 
$options[]      = array(
  'name'        => 'sky_enhancement_panel',
  'title'       => esc_html__( 'Site Enhancement', 'sky-game' ),
  'settings'    => array(

    array(
      'name'          => 'sky_back_to_top',
      'control'       => array(
        'type'        => 'sky_field',
        'options'     => array(
          'type'       => 'switcher',
          'title'      => esc_html__( 'Back To Top Button', 'sky-game' )
        ),
      ),
      'default'       => true
    ),

    // ===== Mailchimp
    array(
      'name'          => 'enable_mailchimp',
      'control'       => array(
        'type'        => 'sky_field',
        'options'     => array(
          'type'       => 'switcher',
          'title'      => esc_html__( 'Enable MailChimp Subscribe', 'sky-game' )
        ),
      ),
      'default'       => false
    ),

    array(
      'name'          => 'mailchimp_api',
      'control'       => array(
        'type'        => 'sky_field',
        'options'     => array(
          'type'       => 'text',
          'title'      => esc_html__( 'MailChimp Settings', 'sky-game' ),
          'dependency' => array( 'enable_mailchimp', '==', 'true' ),
        ),
      )
    ),

    array(
      'name'          => 'range',
      'control'       => array(
        'type'        => 'sky_field',
        'options'     => array(
          'type'       => 'range',
          'title'      => esc_html__( 'MailChimp Settings', 'sky-game' ),
          'min'        => 1,
          'max'        => 100
        ),
      )
    ),
  )
);
