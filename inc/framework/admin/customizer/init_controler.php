<?php
/**
 * Sky Game Theme Customizer.
 *
 * @package Sky Game
 */

// === <<< Register control footer 
	require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/customizer/control_site_enhancement.php';
	require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/customizer/control_header.php';
	require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/customizer/control_footer.php';
	

// === <<< Show all controler
	SKYFramework_Customize::instance( $options );