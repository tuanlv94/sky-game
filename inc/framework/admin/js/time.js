jQuery( function ( $ )
{
	'use strict';

	/**
	 * Update datetime picker element
	 * Used for static & dynamic added elements (when clone)
	 */
	function update()
	{
		var $this = $( this ),
			options = $this.data( 'options' );

		$this.siblings( '.ui-datepicker-append' ).remove();  // Remove appended text
		$this.removeClass( 'hasDatepicker' ).attr( 'id', '' ).timepicker( options );
	}

	// Set language if available
	if ( $.timepicker.regional.hasOwnProperty( SKY_Timepicker.locale ) )
	{
		$.timepicker.setDefaults( $.timepicker.regional[SKY_Timepicker.locale] );
	}
	else if ( $.timepicker.regional.hasOwnProperty( SKY_Timepicker.localeShort ) )
	{
		$.timepicker.setDefaults( $.timepicker.regional[SKY_Timepicker.localeShort] );
	}

	$( '.sky-time' ).each( update );
	$( '.sky-input' ).on( 'clone', '.sky-time', update );
} );
