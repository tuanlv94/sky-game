jQuery( function ( $ )
{
	'use strict';

	$( 'body' ).on( 'change', '.sky-image-select input', function ()
	{
		var $this = $( this ),
			type = $this.attr( 'type' ),
			selected = $this.is( ':checked' ),
			$parent = $this.parent(),
			$others = $parent.siblings();
		if ( selected )
		{
			$parent.addClass( 'sky-active' );
			if ( type === 'radio' )
			{
				$others.removeClass( 'sky-active' );
			}
		}
		else
		{
			$parent.removeClass( 'sky-active' );
		}
	} );
	$( '.sky-image-select input' ).trigger( 'change' );
} );
