jQuery( function ( $ )
{
	'use strict';

	/**
	 * Update datetime picker element
	 * Used for static & dynamic added elements (when clone)
	 */
	function update()
	{
		var $this = $( this ),
			options = $this.data( 'options' );

		$this.siblings( '.ui-datepicker-append' ).remove();         // Remove appended text
		$this.removeClass( 'hasDatepicker' ).attr( 'id', '' ).datetimepicker( options );

	}

	// Set language if available
	if ( $.timepicker.regional.hasOwnProperty( SKY_Datetimepicker.locale ) )
	{
		$.timepicker.setDefaults( $.timepicker.regional[SKY_Datetimepicker.locale] );
	}
	else if ( $.timepicker.regional.hasOwnProperty( SKY_Datetimepicker.localeShort ) )
	{
		$.timepicker.setDefaults( $.timepicker.regional[SKY_Datetimepicker.localeShort] );
	}

	$( ':input.sky-datetime' ).each( update );
	$( '.sky-input' ).on( 'clone', ':input.sky-datetime', update );
} );
