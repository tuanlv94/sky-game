<?php
require SKY_INC_DIR . 'field.php';
require SKY_INC_DIR . 'field-multiple-values.php';

// Field classes
foreach ( glob( SKY_FIELDS_DIR . '*.php' ) as $file ){
	require $file;
}

// Meta box class
require SKY_INC_DIR . 'meta-box.php';

// Validation module
require SKY_INC_DIR . 'validation.php';

// Helper function to retrieve meta value
require SKY_INC_DIR . 'helpers.php';

// Main file
require SKY_INC_DIR . 'init.php';
