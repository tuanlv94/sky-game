<?php
/* -------------------------------------------------------
 * Create functions sky_shortcode_offered_client
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_shortcode_offered_client' ) ) :
	
	function sky_shortcode_offered_client( $atts, $content = null ) {

		extract(shortcode_atts(array(
			'title'                  => '',
			'sub_title'              => ''
		), $atts));
		$terms = get_terms( sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' ), array(
		    'orderby'    => 'count',
		    'hide_empty' => 0
		) );

		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :

			echo '<div class="sky-xs-12 sky-sm-3 sky_offered_client">';
				if ( !empty($title) ) :
					echo '<h2 class="offered_title">' . $title . '</h2>';
					echo '<h3 class="sub_offered_title">' . $sub_title . '</h3>';
				endif;

		    echo '</div>';

		    echo '<div class="sky-xs-12 sky-sm-9 sky_offered_client_list">';

			foreach( $terms as $term ) :

				?>
	               <div class="sky_offered_client_item">
	                   <a href="<?php echo get_term_link( $term ) ?>" title="<?php echo $term->name ?>">
	                       <img src="<?php echo wp_get_attachment_url( $term->term_id ); ?>" title="<?php echo $term->name ?>" alt="<?php echo $term->name ?>" width="60px" height="60px" />
	                   </a>
	                </div>
	            <?php

			endforeach;
			echo '</div>';
		    echo '<script type="text/javascript">
					jQuery(document).ready(function($) {
						var client = $(".sky_offered_client_list");
					  	client.owlCarousel({
							items : 3,
							autoPlay: 7500,
					  	});
					});
				</script>';
			/* Restore original Post Data */
				wp_reset_postdata();

		endif;

	}

	add_shortcode('sky_offered_client', 'sky_shortcode_offered_client');

endif;

/** ====== END sky_list_job ====== **/

