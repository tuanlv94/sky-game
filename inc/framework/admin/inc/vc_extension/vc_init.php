<?php
// Incremental ID Counter for Templates
if ( ! function_exists( 'sky_vc_elements_id_increment' ) ) :
    function sky_vc_elements_id_increment() {
        static $count = 0; $count++;
        return $count;
    }
endif;
// Function for handle element's visibility
if ( ! function_exists( 'sky_visibility_class' ) ) :
    function sky_visibility_class( $visibility = '' ) {
        switch ($visibility) {
            case 'hidden-phone':
                return ' hidden-xs';
            case 'hidden-tablet':
                return ' hidden-sm';
            case 'hidden-pc':
                return ' hidden-md hidden-lg';
            case 'visible-phone':
                return ' visible-xs';
            case 'visible-tablet':
                return ' visible-sm';
            case 'visible-pc':
                return ' visible-md visible-lg';
            default:
                return '';
        }
    }
endif;
if ( class_exists('WPBakeryVisualComposerAbstract') ):
    if ( ! function_exists( 'sky_includevisual' ) ) :
        function sky_includevisual(){
            $dir = dirname(__FILE__);
            // VC Templates
            $vc_templates_dir = SKY_THEME_INCLUDES_FRAMEWORK . '/admin/inc/vc_extension/shortcodes/vc_templates/';
            vc_set_shortcodes_templates_dir($vc_templates_dir);

            // require file
            sky_autoload( $dir . '/map' );
            sky_autoload( $dir . '/shortcodes' );
        }
        add_action('init', 'sky_includevisual');
    endif;
endif;

/* -------------------------------------------------------
 * Create functions vc_sky_get_list_category
 * ------------------------------------------------------- */

if ( ! function_exists( 'vc_sky_get_list_category' ) ) :
    
    function vc_sky_get_list_category( $type = 'post' ) {
        
        if ( $type == 'post' ) :
            $args_post = array(
                'hide_empty' => false,
                'orderby'    => 'name',
                'order'      => 'ASC'
            );
            $category_post = array( esc_html__( 'All category', 'sky-game' ) => 'all' );
            $category_post_list = get_categories( $args_post );

            if ( is_array( $category_post_list ) && ! empty( $category_post_list ) ) {
                foreach ( $category_post_list as $category_details ) {
                    $category_post[ $category_details->name ] = $category_details->slug; 
                }
            }

            return $category_post;

        elseif ( $type == 'game' ) :

            $category_game_slug = sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' );
            $args_game = array(
                'hide_empty' => false,
                'orderby'    => 'name',
                'order'      => 'ASC'
            );
            $category_game = array( esc_html__( 'All category', 'sky-game' ) => 'all' );
            $category_game_list = get_terms( $category_game_slug, $args_game );

            if ( is_array( $category_game_list ) && ! empty( $category_game_list ) ) {
                foreach ( $category_game_list as $category_details ) {
                    $category_game[ $category_details->name ] = $category_details->slug; 
                }
            }

            return $category_game;

        endif;

    }

endif;

/** ====== END vc_sky_get_list_category ====== **/