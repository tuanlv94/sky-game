<?php
/**
 * Creat VC Sky Offered Client
 */
vc_map( 
	array( 
		'base'        => 'sky_offered_client', 
		'name'        => esc_html__( 'Sky Offered Client', 'sky-game' ), 
		'weight'      => 809,
		'category'    => $category_name, 
		'description' => '', 
		'params'      => array( 
			array( 
				'param_name'  => 'title', 
				'heading'     => esc_html__( 'Title', 'sky-game' ), 
				'type'        => 'textfield', 
				'admin_label' => true, 
				'holder'      => 'div'
			),
			array( 
				'param_name'  => 'sub_title', 
				'heading'     => esc_html__( 'Sub Title', 'sky-game' ), 
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		) 
	) 
);