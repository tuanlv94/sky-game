<?php
/**
 * Creat VC Sky Recent Post
 */
vc_map( 
	array( 
		'base'        => 'sky_recent_post', 
		'name'        => esc_html__( 'Sky Recent Post', 'sky-game' ), 
		'weight'      => 809,
		'category'    => $category_name, 
		'style' => '', 
		'params'      => array( 
			array( 
				'param_name'  => 'title', 
				'heading'     => esc_html__( 'Title', 'sky-game' ), 
				'type'        => 'textfield', 
				'admin_label' => true, 
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'show',
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Show', 'sky-game' ),
				'std'         => 'post',
				'value'       => 
					array(
						esc_html__( 'Post', 'sky-game' ) => 'post',
						esc_html__( 'Game', 'sky-game' ) => 'game'
					),
				'holder'      => 'div'
			),
			array(
				'param_name' => 'category_post',
				'heading'    => __( 'Category Post', 'sky-game' ),
				'dependency' => array( 'element' => 'show', 'value' => array( 'post' ) ),
				'type'       => 'dropdown',
				'std'        => 'all',
				'value' 	 => vc_sky_get_list_category('post'),
				'holder'     => 'div'
			),
			array(
				'param_name' => 'category_game',
				'heading'    => __( 'Category Game', 'sky-game' ),
				'dependency' => array( 'element' => 'show', 'value' => array( 'game' ) ),
				'type'       => 'dropdown',
				'std'        => 'all',
				'value' 	 => vc_sky_get_list_category('game'),
				'holder'     => 'div'
			),
			array(
				'param_name'  => 'style',
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Display Style', 'sky-game' ),
				'std'         => 'list',
				'value'       => 
					array(
						esc_html__( 'List', 'sky-game' ) => 'list',
						esc_html__( 'Grid', 'sky-game' ) => 'grid'
					),
				'holder'      => 'div'
			),
			array(
				'param_name' => 'column',
				'heading'    => esc_html__( 'Column', 'sky-game' ),
				'dependency' => array( 'element' => 'style', 'value' => array( 'grid' ) ),
				'type'       => 'dropdown',
				'std'        => '5',
				'value'      => array(
					esc_html__( 'Two Column', 'sky-game' )   => '6',
					esc_html__( 'Three Column', 'sky-game' ) => '4', 
					esc_html__( 'Four Column', 'sky-game' )  => '3',
					esc_html__( 'Five Column', 'sky-game' )  => '5',
					esc_html__( 'Six Column', 'sky-game' )   => '2',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'posts_per_page',
				'heading'     => esc_html__( 'Posts per page', 'sky-game' ),
				'type'        => 'textfield',
				'admin_label' => true,
				'value'       => '10',
				'holder'      => 'div'
			),
			array(
				'param_name' => 'orderby',
				'heading'    => esc_html__( 'Order by', 'sky-game' ),
				'type'       => 'dropdown',
				'std'        => 'date',
				'value'      => array(
					esc_html__( 'Date', 'sky-game' )    => 'date',
					esc_html__( 'Popular', 'sky-game' ) => 'view',
					esc_html__( 'Random', 'sky-game' )  => 'rand' 
				)
			),
			array(
				'param_name' => 'order',
				'heading'    => esc_html__( 'Sort by', 'sky-game' ),
				'type'       => 'dropdown',
				'dependency' => array( 'element' => 'orderby', 'value' => array( 'date', 'view' ) ),
				'std'        => 'desc',
				'value'      => array(
					esc_html__( 'Recent', 'sky-game' ) => 'desc',
					esc_html__( 'Older', 'sky-game' ) => 'asc'
				)
			),
			array(
				'param_name'  => 'show_pagination',
				'heading'     => esc_html__( 'Show Pagination', 'sky-game' ),
				'description' => esc_html__( 'Show/hide Pagination.', 'sky-game' ),
				'type'        => 'dropdown',
				'std'         => 'no',
				'value'       => array(
					esc_html__( 'Hide', 'sky-game' ) => 'no',
					esc_html__( 'Show', 'sky-game' ) => 'yes' 
				)
			),
			array(
				'param_name'  => 'show_readmore',
				'heading'     => esc_html__( 'Show Readmore', 'sky-game' ),
				'dependency'  => array( 'element' => 'show', 'value' => array( 'post' ) ),
				'description' => esc_html__( 'Show/hide Readmore.', 'sky-game' ),
				'type'        => 'dropdown',
				'std'         => 'yes',
				'value'       => array(
					esc_html__( 'Show', 'sky-game' ) => 'yes',
					esc_html__( 'Hide', 'sky-game' ) => 'no',
				)
			),
			array(
				'param_name'    => 'show_link_download',
				'heading'       => __( 'Show Link Download', 'sky-game' ),
				'dependency'    => array( 'element' => 'show', 'value' => array( 'game' ) ),
				'description'   => __( 'Show/hide link download.', 'sky-game' ),
				'type'          => 'dropdown',
				'std'         	=> 'yes',
				'value'         => array(
					__( 'Show', 'sky-game' ) => 'yes',
					__( 'Hide', 'sky-game' ) => 'no'
				)
			),
			array(
				'param_name'    => 'show_support',
				'heading'       => __( 'Show Support', 'sky-game' ),
				'dependency'    => array( 'element' => 'show', 'value' => array( 'game' ) ),
				'description'   => __( 'Show/hide', 'sky-game' ),
				'type'          => 'dropdown',
				'std'         	=> 'yes',
				'value'         => array(
					__( 'Show', 'sky-game' ) => 'yes',
					__( 'Hide', 'sky-game' ) => 'no'
				)
			),
			array(
				'param_name'    => 'show_excerpt',
				'heading'       => __( 'Show Excerpt', 'sky-game' ),
				'dependency'    => array( 'element' => 'show', 'value' => array( 'post' ) ),
				'description'   => __( 'Show/hide excerpt content.', 'sky-game' ),
				'type'          => 'dropdown',
				'std'         	=> 'yes',
				'value'         => array(
					__( 'Show', 'sky-game' ) => 'yes',
					__( 'Hide', 'sky-game' ) => 'no'
				)
			),
			array(
				'param_name'    => 'show_thumbnail',
				'heading'       => __( 'Show Thumbnail', 'sky-game' ),
				'description'   => __( 'Show/hide Thumbnail.', 'sky-game' ),
				'type'          => 'dropdown',
				'std'         	=> 'yes',
				'value'         => array(
					__( 'Show', 'sky-game' ) => 'yes',
					__( 'Hide', 'sky-game' ) => 'no'
				)
			),
		) 
	) 
);