<?php
/**
 * SKY Visual Composer Add-ons
 *
 * Customize Visual Composer to suite SKY Framework
 *
 * @package    SKY Framework
 * @subpackage SKY Visual Composer Add-on
 */
//
// Variables.
//
$category_name                  = esc_html__( 'Sky Game', 'sky-game' );

// custom [row]
vc_add_param('vc_row', array(
        "type"        =>  "checkbox",
        "admin_label" =>  true,
        "heading"     =>  "Using Container",
        "param_name"  =>  "container_width",
        "description" =>  esc_html__('If checked container will be set to width 1170px for content.','sky-game'),
        'weight'      => 1,
        'value'       => array( esc_html__( 'Yes', 'sky-game' ) => 'yes' )
    )
);
