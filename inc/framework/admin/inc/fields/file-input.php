<?php
if ( ! class_exists( 'SKY_File_Input_Field' ) )
{
	class SKY_File_Input_Field extends SKY_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			// Make sure scripts for new media uploader in WordPress 3.5 is enqueued
			wp_enqueue_media();
			wp_enqueue_script( 'sky-file-input', SKY_JS_URL . 'file-input.js', array( 'jquery' ), SKY_VER, true );
			wp_localize_script( 'sky-file-input', 'skyFileInput', array(
				'frameTitle' => __( 'Select File', 'sky-game' ),
			) );
		}

		/**
		 * Get field HTML
		 *
		 * @param mixed $meta
		 * @param array $field
		 *
		 * @return string
		 */
		static function html( $meta, $field )
		{
			return sprintf(
				'<input type="text" class="sky-file-input" name="%s" id="%s" value="%s" placeholder="%s" size="%s">
				<a href="#" class="sky-file-input-select button-primary">%s</a>
				<a href="#" class="sky-file-input-remove button %s">%s</a>',
				$field['field_name'],
				$field['id'],
				$meta,
				$field['placeholder'],
				$field['size'],
				__( 'Select', 'sky-game' ),
				$meta ? '' : 'hidden',
				__( 'Remove', 'sky-game' )
			);
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = wp_parse_args( $field, array(
				'size'        => 30,
				'placeholder' => '',
			) );

			return $field;
		}
	}
}
