<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Make sure "text" field is loaded
require_once SKY_FIELDS_DIR . 'text.php';

if ( ! class_exists( 'SKY_Password_Field' ) )
{
	class SKY_Password_Field extends SKY_Text_Field
	{
		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = parent::normalize_field( $field );

			$field['attributes']['type']  = 'password';
			$field['attributes']['class'] = 'sky-password';

			return $field;
		}
	}
}
