<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Make sure "select" field is loaded
require SKY_FIELDS_DIR . 'select.php';

if ( ! class_exists( 'SKY_Select_Advanced_Field' ) )
{
	class SKY_Select_Advanced_Field extends SKY_Select_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style( 'select2', SKY_CSS_URL . 'select2/select2.css', array(), '3.2' );
			wp_enqueue_style( 'sky-select-advanced', SKY_CSS_URL . 'select-advanced.css', array(), SKY_VER );

			wp_register_script( 'select2', SKY_JS_URL . 'select2/select2.min.js', array(), '3.2', true );
			wp_enqueue_script( 'sky-select', SKY_JS_URL . 'select.js', array(), SKY_VER, true );
			wp_enqueue_script( 'sky-select-advanced', SKY_JS_URL . 'select-advanced.js', array( 'select2', 'sky-select' ), SKY_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param mixed $meta
		 * @param array $field
		 *
		 * @return string
		 */
		static function html( $meta, $field )
		{
			$html = sprintf(
				'<select class="sky-select-advanced" name="%s" id="%s" size="%s"%s data-options="%s">',
				$field['field_name'],
				$field['id'],
				$field['size'],
				$field['multiple'] ? ' multiple' : '',
				esc_attr( wp_json_encode( $field['js_options'] ) )
			);

			$html .= self::options_html( $field, $meta );

			$html .= '</select>';

			$html .= self::get_select_all_html( $field['multiple'] );

			return $html;
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = parent::normalize_field( $field );

			$field = wp_parse_args( $field, array(
				'js_options' => array(),
			) );

			$field['js_options'] = wp_parse_args( $field['js_options'], array(
				'allowClear'  => true,
				'width'       => 'off',
				'placeholder' => $field['placeholder'],
			) );

			return $field;
		}
	}
}
