<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * Framework admin enqueue style and scripts
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if( ! function_exists( 'sky_admin_enqueue_scripts' ) ) {
  function sky_admin_enqueue_scripts() {

    // admin utilities
    wp_enqueue_media();

    // wp core styles
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_style( 'wp-jquery-ui-dialog' );

    // framework core styles
    wp_enqueue_style( 'sky-game', SKY_URI .'/assets/css/sky-framework.css', array(), '1.0.0', 'all' );
    wp_enqueue_style( 'font-awesome', SKY_URI .'/assets/css/font-awesome.css', array(), '4.2.0', 'all' );

    if ( is_rtl() ) {
      wp_enqueue_style( 'sky-framework-rtl', SKY_URI .'/assets/css/sky-framework-rtl.css', array(), '1.0.0', 'all' );
    }

    // wp core scripts
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( 'jquery-ui-dialog' );
    wp_enqueue_script( 'jquery-ui-sortable' );
    wp_enqueue_script( 'jquery-ui-accordion' );

    // framework core scripts
    wp_enqueue_script( 'sky-plugins',    SKY_URI .'/assets/js/sky-plugins.js',    array(), '1.0.0', true );
    wp_enqueue_script( 'sky-game',  SKY_URI .'/assets/js/sky-framework.js',  array( 'sky-plugins' ), '1.0.0', true );

  }
  add_action( 'admin_enqueue_scripts', 'sky_admin_enqueue_scripts' );
}
