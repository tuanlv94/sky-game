<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * Get icons from admin ajax
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if( ! function_exists( 'sky_get_icons' ) ) {
  function sky_get_icons() {

    $jsons = glob( SKY_DIR_STAR . '/fields/icon/*.json' );

    if( ! empty( $jsons ) ) {

      foreach ( $jsons as $path ) {

        $object = sky_get_icon_fonts( 'fields/icon/'. basename( $path ) );

        if( is_object( $object ) ) {

          echo ( count( $jsons ) >= 2 ) ? '<h4 class="sky-icon-title">'. $object->name .'</h4>' : '';

          foreach ( $object->icons as $icon ) {
            echo '<a class="sky-icon-tooltip" data-icon="'. $icon .'" data-title="'. $icon .'"><span class="sky-icon sky-selector"><i class="'. $icon .'"></i></span></a>';
          }

        } else {
          echo '<h4 class="sky-icon-title">'. esc_html__( 'Error! Can not load json file.', 'sky-game' ) .'</h4>';
        }

      }

    }

    do_action( 'sky_add_icons' );

    die();
  }
  add_action( 'wp_ajax_sky-get-icons', 'sky_get_icons' );
}

/**
 *
 * Set icons for wp dialog
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if( ! function_exists( 'sky_set_icons' ) ) {
  function sky_set_icons() {

    echo '<div id="sky-icon-dialog" class="sky-dialog" title="'. esc_html__( 'Add Icon', 'sky-game' ) .'">';
    echo '<div class="sky-dialog-header sky-text-center"><input type="text" placeholder='. esc_html__( 'Search a Icon...', 'sky-game' ) .'" class="sky-icon-search" /></div>';
    echo '<div class="sky-dialog-load"><div class="sky-icon-loading">'. esc_html__( 'Loading...', 'sky-game' ) .'</div></div>';
    echo '</div>';

  }
  add_action( 'admin_footer', 'sky_set_icons' );
  add_action( 'customize_controls_print_footer_scripts', 'sky_set_icons' );
}
