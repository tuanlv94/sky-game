<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * Field: Backup
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
class SKYFramework_Option_backup extends SKYFramework_Options {

  public function __construct( $field, $value = '', $unique = '' ) {
    parent::__construct( $field, $value, $unique );
  }

  public function output() {

    echo $this->element_before();

    echo '<textarea name="'. $this->unique .'[import]"'. $this->element_class() . $this->element_attributes() .'></textarea>';
    submit_button( esc_html__( 'Import a Backup', 'sky-game' ), 'primary sky-import-backup', 'backup', false );
    echo '<small>( '. esc_html__( 'copy-paste your backup string here', 'sky-game' ).' )</small>';

    echo '<hr />';

    echo '<textarea name="_nonce"'. $this->element_class() . $this->element_attributes() .' disabled="disabled">'. sky_encode_string( get_option( $this->unique ) ) .'</textarea>';
    echo '<a href="'. admin_url( 'admin-ajax.php?action=sky-export-options' ) .'" class="button button-primary" target="_blank">'. esc_html__( 'Export and Download Backup', 'sky-game' ) .'</a>';
    echo '<small>-( '. esc_html__( 'or', 'sky-game' ) .' )-</small>';
    submit_button( esc_html__( '!!! Reset All Options !!!', 'sky-game' ), 'sky-warning-primary sky-reset-confirm', $this->unique . '[resetall]', false );
    echo '<small class="sky-text-warning">'. esc_html__( 'Please be sure for reset all of framework options.', 'sky-game' ) .'</small>';
    echo $this->element_after();

  }

}
