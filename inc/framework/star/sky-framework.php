<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * ------------------------------------------------------------------------------------------------
 *
 * Sky Framework
 *
 */

// ------------------------------------------------------------------------------------------------
require plugin_dir_path( __FILE__ ) .'/sky-framework-path.php';
// ------------------------------------------------------------------------------------------------

if( ! function_exists( 'sky_framework_init' ) && ! class_exists( 'SKYFramework' ) ) {
  function sky_framework_init() {

    // active modules
    defined( 'SKY_ACTIVE_FRAMEWORK' )  or  define( 'SKY_ACTIVE_FRAMEWORK',  false );
    defined( 'SKY_ACTIVE_METABOX'   )  or  define( 'SKY_ACTIVE_METABOX',    false );
    defined( 'SKY_ACTIVE_SHORTCODE' )  or  define( 'SKY_ACTIVE_SHORTCODE',  true );
    defined( 'SKY_ACTIVE_CUSTOMIZE' )  or  define( 'SKY_ACTIVE_CUSTOMIZE',  true );

    // helpers
    sky_locate_template( 'functions/deprecated.php'     );
    sky_locate_template( 'functions/helpers.php'        );
    sky_locate_template( 'functions/actions.php'        );
    sky_locate_template( 'functions/enqueue.php'        );
    sky_locate_template( 'functions/sanitize.php'       );
    sky_locate_template( 'functions/validate.php'       );

    // classes
    sky_locate_template( 'classes/abstract.class.php'   );
    sky_locate_template( 'classes/metabox.class.php'    );
    sky_locate_template( 'classes/shortcode.class.php'  );
    sky_locate_template( 'classes/customize.class.php'  );

    // configs
    sky_locate_template( 'config/metabox.config.php'    );
    sky_locate_template( 'config/shortcode.config.php'  );
    require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/customizer/init_controler.php';

  }
  add_action( 'init', 'sky_framework_init', 10 );
}
