<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// CUSTOMIZE SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options              = array();

// -----------------------------------------
// Customize Core Fields                   -
// -----------------------------------------
$options[]            = array(
  'name'              => 'wp_core_fields',
  'title'             => 'WP Core Fields',
  'settings'          => array(

    // color
    array(
      'name'          => 'color_option_with_default',
      'default'       => '#d80039',
      'control'       => array(
        'label'       => 'Color',
        'type'        => 'color',
      ),
    ),

    // text
    array(
      'name'          => 'text_option',
      'control'       => array(
        'label'       => 'Text',
        'type'        => 'text',
      ),
    ),

    // text with default
    array(
      'name'          => 'text_option_with_default',
      'default'       => 'bla bla bla',
      'control'       => array(
        'label'       => 'Text with Default',
        'type'        => 'text',
      ),
    ),

    // textarea
    array(
      'name'          => 'textarea_option',
      'control'       => array(
        'label'       => 'Textarea',
        'type'        => 'textarea',
      ),
    ),

    // checkbox
    array(
      'name'          => 'checkbox_option',
      'control'       => array(
        'label'       => 'Checkbox',
        'type'        => 'checkbox',
      ),
    ),

    // radio
    array(
      'name'          => 'radio_option',
      'control'       => array(
        'label'       => 'Radio',
        'type'        => 'radio',
        'choices'     => array(
          'key1'      => 'value 1',
          'key2'      => 'value 2',
          'key3'      => 'value 3',
        )
      ),
    ),

    // select
    array(
      'name'          => 'select_option',
      'control'       => array(
        'label'       => 'Select',
        'type'        => 'select',
        'choices'     => array(
          ''          => '- Select a value -',
          'key1'      => 'value 1',
          'key2'      => 'value 2',
          'key3'      => 'value 3',
        )
      ),
    ),

    // dropdown-pages
    array(
      'name'          => 'dropdown_pages_option',
      'control'       => array(
        'label'       => 'Dropdown-Pages',
        'type'        => 'dropdown-pages',
      ),
    ),

    // upload
    array(
      'name'          => 'upload_option',
      'control'       => array(
        'label'       => 'Upload',
        'type'        => 'upload',
      ),
    ),

    // image
    array(
      'name'          => 'image_option',
      'control'       => array(
        'label'       => 'Image',
        'type'        => 'image',
      ),
    ),

    // media
    array(
      'name'          => 'media_option',
      'control'       => array(
        'label'       => 'Media',
        'type'        => 'media',
      ),
    ),

  )
);

// -----------------------------------------
// Customize SKY Fields               -
// -----------------------------------------

require SKY_THEME . '/inc/customizer.php';

// -----------------------------------------
// Customize Panel Options Fields          -
// -----------------------------------------
$options[]            = array(
  'name'              => 'sky_panel_1',
  'title'             => 'SKY Framework Panel',
  'description'       => 'SKY customize panel description.',
  'sections'          => array(

    // begin: section
    array(
      'name'          => 'section_1',
      'title'         => 'Section 1',
      'settings'      => array(

        array(
          'name'      => 'color_option_1',
          'default'   => '#ffbc00',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => 'Color Option 1',
            ),
          ),
        ),

        array(
          'name'      => 'color_option_2',
          'default'   => '#2ecc71',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => 'Color Option 2',
            ),
          ),
        ),

        array(
          'name'      => 'color_option_3',
          'default'   => '#e74c3c',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => 'Color Option 3',
            ),
          ),
        ),

        array(
          'name'      => 'color_option_4',
          'default'   => '#3498db',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => 'Color Option 4',
            ),
          ),
        ),

        array(
          'name'      => 'color_option_5',
          'default'   => '#555555',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => 'Color Option 5',
            ),
          ),
        ),

      ),
    ),
    // end: section

    // begin: section
    array(
      'name'          => 'section_2',
      'title'         => 'Section 2',
      'settings'      => array(

        array(
          'name'      => 'text_option_1',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'text',
              'title' => 'Text Option 1',
            ),
          ),
        ),

        array(
          'name'      => 'text_option_2',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'text',
              'title' => 'Text Option 2',
            ),
          ),
        ),

        array(
          'name'      => 'text_option_3',
          'control'   => array(
            'type'    => 'sky_field',
            'options' => array(
              'type'  => 'text',
              'title' => 'Text Option 3',
            ),
          ),
        ),

      ),
    ),
    // end: section

  ),
  // end: sections

);

SKYFramework_Customize::instance( $options );
