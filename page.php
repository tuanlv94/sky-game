<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sky Game
 */

get_header(); ?>
<div <?php post_class( 'sky-container' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php sky_get_layout( 'content-page' ); ?>

	<?php endwhile; // End of the loop. ?>

	<?php get_sidebar(); ?>
</div> 
<?php get_footer(); ?>
